#ifndef _STATE_MANAGER_H
#define _STATE_MANAGER_H

#define MAX_STATES              10

#include <stdbool.h>
#include <stdint.h>

#include "State.h"

typedef struct StateManager {
    uint32_t            refNow;
    uint32_t            refNext;
    uint32_t            numStates;
    State_t *           states[MAX_STATES];

} StateManager_t;

StateManager_t *        StateManager_new();
void                    StateManager_delete(StateManager_t * sm);

void                    StateManager_addState(StateManager_t * sm, State_t * state);
void                    StateManager_begin(StateManager_t * sm);
void                    StateManager_tick (StateManager_t * sm);
void                    StateManager_goto (StateManager_t * sm, uint32_t ref);

#endif
