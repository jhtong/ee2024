#include <assert.h>
#include <stdlib.h>

#include "Calculator.h"
#include "new.h"
#include "new.r"

struct Calculator {
    const void * class;
    /** Accessible variables go here **/
    int a;
    int b;
};

static void * Calculator_ctor (void * _self, va_list * app) 
{
    struct Calculator * self = _self;
    self->a = va_arg(*app, int);
    self->b = va_arg(*app, int);
    /** Begin alloc internal vars here **/
    /**                                **/
    /** End alloc internal vars here   **/
    return self;
}

static void * Calculator_dtor (void * _self) 
{
    struct Calculator * self = _self;
    /** Begin Free internal vars here **/
    /**                               **/
    /** End Free internal vars here   **/
    return self;
}

static void * Calculator_clone(const void * _self) 
{
    const struct Calculator * self = _self;
    return new(Calculator, self->a, self->b);

}

static int Calculator_differ(const void * _self, const void * _b)
{
    const struct Calculator * self = _self;
    const struct Calculator * b = _b;
    if (self == b) {
        return 0;
    }
    if (!b || b->class != Calculator) {
        return 0;
    }
    // TODO: return dummy here
    return 1;
}

static const struct Class _Calculator = {
    sizeof(struct Calculator),
    Calculator_ctor, 
    Calculator_dtor,
    Calculator_clone,
    Calculator_differ
};

/** Expose class **/
const void * Calculator = &_Calculator;


/***********************************************/
/****   Public custom functions go here     ****/
/***********************************************/

int Calculator_add(void * self)
{
    const struct Calculator * _self = self;
    return _self->a + _self->b;

}
