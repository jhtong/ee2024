#include <stdio.h>
#include "State.h"
#include "StateManager.h"

StateManager_t * sm;

void tickHandler() { 
    puts("state 0: entered tick"); 
    StateManager_goto(sm, 1);
}
void enterHandler() { puts("state 0: entered Enter"); }
void endHandler() { puts("state 0: entered End"); }

void tickHandler1() { 
    puts("state 1: entered tick"); 
    StateManager_goto(sm, 0);
}
void enterHandler1() { puts("state 1: entered Enter"); }
void endHandler1() { puts("state 1: entered End"); }

int main() 
{
    State_t * state = State_new(tickHandler, enterHandler, endHandler);
    State_t * state1 = State_new(tickHandler1, enterHandler1, endHandler1);

    sm = StateManager_new();
    StateManager_addState(sm, state);
    StateManager_addState(sm, state1);
    StateManager_begin(sm);

    return 0;
}

