#include "statesWireless.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "StateManager.h"
#include "FFS_Config.h"
#include "Vector_Data.h"
#include "FFSClock.h"

static Message_t * lastMsg;

// reference to wireless manager in main.c
extern StateManager_t * wm;

void wState_authenticateTick() {
    //timeout 
    if(Terminal_getTimeElapsed(tm) > 5000 && !Terminal_hasMessage(tm)) {
        Terminal_resetTime(tm);
        Terminal_send(tm, "RDY 013 \r");
    }

    // return if no message
    if (!Terminal_hasMessage(tm)) return;

    // else continue
    lastMsg = Terminal_retrieve(tm);
    char ** tokens = lastMsg->tokens;
    
    if (strcmp(tokens[0], "RNACK") == 0) {
    	puts("not acknowledged.");
        Terminal_send(tm, "RDY 013");
    }

    else if (strcmp(tokens[0], "RACK") == 0) {
    	puts("acknowledged!!!");
        Terminal_send(tm, "HSHK 013");
        StateManager_goto(wm, 1);
    }

    Message_delete(lastMsg);
}

void wState_authenticateBegin() {
    Terminal_send(tm, "RDY 013");
}

void wState_authenticateEnd() {
}

/** Active state (after handshaking **************************************************/

/** NOTE: Output string must be deleted!  **/
char * intToStr(uint32_t i, uint8_t len) {
	char * str = calloc(len, sizeof(char));
	sprintf(str, "%d", (int) i);
	return str;
}

static char * makeReportString() {
	char * freqStr = intToStr((uint32_t) FFSData.vibrationFreq, 2);
	char * reportStr = calloc(1, 25 * sizeof(char));

	strcat(reportStr, "REPT ");
	strcat(reportStr, "013 ");
	strcat(reportStr, freqStr);

	if(FFSData.warningOn)
		strcat(reportStr, " WARNING");

	free(freqStr);
	return reportStr;
}

static void pingIfNeeded() {

	uint32_t interval;
	interval = FFSData.warningOn ? PROTOCOL_REPORT_TIME_WARNING : PROTOCOL_REPORT_TIME_NORMAL;

	static uint32_t lastPingTime = 0;
	if ((SysTick_getMsElapsed() - lastPingTime) < interval)
		return;
	lastPingTime = SysTick_getMsElapsed();
	puts("reporting!");

	char * reportStr = makeReportString();
	Terminal_send(tm, reportStr);
	free(reportStr);

}

void wState_activeTick() {
	pingIfNeeded(); 				// send data over

	if (!Terminal_hasMessage(tm)) return;

    // else continue
    lastMsg = Terminal_retrieve(tm);
    char ** tokens = lastMsg->tokens;

    if (strcmp(tokens[0], "RSTC") == 0) {
    	puts("told to calibrate");
        Terminal_send(tm, "CACK");
    	//TODO: Goto calibrate mode

    }

    if (strcmp(tokens[0], "RSTS") == 0) {
    	puts("told to reset");
        Terminal_send(tm, "SACK");
    	//TODO: Goto reset mode
    }

    Message_delete(lastMsg);
}

void wState_activeBegin() {

}

void wState_activeEnd() {

}

void init_wirelessStates() {
    tm = Terminal_new();
    wState_auth = State_new(
            wState_authenticateTick, 
            wState_authenticateBegin, 
            wState_authenticateEnd);

    wState_active = State_new(
    		wState_activeTick,
    		wState_activeBegin,
    		wState_activeEnd);
}

void free_wirelessStates() {
    State_delete(wState_auth);
    State_delete(wState_active);
    Terminal_delete(tm);
}

