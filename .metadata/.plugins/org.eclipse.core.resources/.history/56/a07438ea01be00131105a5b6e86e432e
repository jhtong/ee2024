/*
 * Terminal.c
 *
 *  Created on: Mar 29, 2014
 *      Author: joel
 */

#include "Terminal.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#include "LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_uart.h"

#include "FFS_Config.h"
#include "FFSClock.h"

#define MAX_CHUNKS					10

/** strdup function from GNU-C **/
char *strdup (const char *s) {
    char *d = malloc (strlen (s) + 1);   // Space for length plus nul
    if (d == NULL) return NULL;          // No memory
    strcpy (d,s);                        // Copy the characters
    return d;                            // Return the new string
}


Message_t * Message_new(Author_t to, Author_t from, const char * payload) {

    // clone string
    char * _payload;
    _payload = calloc(1, strlen(payload) + 1);
    strcpy(_payload, payload);

    // clean up string's \r\n
    uint32_t u;
    for (u = 0; u < strlen(_payload); u++) {
    	if (_payload[u] == '\r' || payload[u] == '\n')
    		_payload[u] = ' ';
    }

    // tokenize string
    uint8_t length = 0;
    char ** tmpMsg;
    char * chunk = NULL;
    tmpMsg = calloc(MAX_CHUNKS, sizeof(char *));

    chunk = strtok(_payload, " ");
    tmpMsg[0] = strdup(chunk);
    if (chunk != NULL) length++;

    while((chunk = strtok(NULL, " "))) {
        tmpMsg[length] = strdup(chunk);
        length++;
    }

    // free unused memory
    uint8_t i;
    for( i = length; i < (uint8_t) MAX_CHUNKS; i++)
        free(tmpMsg[i]);
    free(_payload);
    free(chunk);

    // create message
    Message_t * self = calloc(1, sizeof(Message_t));
    self->to = to;
    self->from = from;
    self->length = length;
    self->tokens = tmpMsg;

    return self;
}

void Message_delete(Message_t * self) {
    uint8_t i = 0;
    for(i = 0; i < self->length; i++) {
        free(self->tokens[i]);
    }
    free(self);
}

/** IMPT: Remember to free return string **/
char * Message_get(Message_t * self, _Bool addNewlineReturn) {
    char * buffer;
    buffer = calloc(1, UART_BUFF_SIZE);
    uint8_t i = 0;
    strcat(buffer, self->tokens[i]);
    for (i = 1; i < self->length; i++) {
        strcat(buffer, " ");
        strcat(buffer, self->tokens[i]);
    }
    if (addNewlineReturn) strcat(buffer, "\r\n");
    return buffer;
}

/**
 *******************************************************************************************************
 * XBee UART Stuff *************************************************************************************
 *******************************************************************************************************
 */

void UART3_IRQHandler() {
	UART3_StdIntHandler();
}

// workaround to create a passable instance to interrupt callback
static Terminal_t * termSingleton;
static char buffer[UART_BUFF_SIZE];

static void uart_rcv_callback() {
	puts("received");
	memset(buffer, 0, UART_BUFF_SIZE); 				// clear buffer first
    uint32_t len = UART_Receive(LPC_UART3, (uint8_t *) buffer, (uint32_t) UART_BUFF_SIZE, NONE_BLOCKING);
    Terminal_harvest(termSingleton, (uint8_t *) buffer, len);
    printf("%d\n", (int) len); 				// size of data.  TODO: remove this log statement
}

static void pinsel_uart3(void) {
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = 2;
	PinCfg.Pinnum = 0;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 1;
	PINSEL_ConfigPin(&PinCfg);
}

void init_uart(void) {
	UART_CFG_Type uartCfg;
	uartCfg.Baud_rate = 115200;
	uartCfg.Databits = UART_DATABIT_8;
	uartCfg.Parity = UART_PARITY_NONE;
	uartCfg.Stopbits = UART_STOPBIT_1;
	//pin select for uart3;
	pinsel_uart3();
	UART_Init(LPC_UART3, &uartCfg);
	UART_SetupCbs(LPC_UART3, 0, uart_rcv_callback);
	UART_TxCmd(LPC_UART3, ENABLE);
	UART_IntConfig(LPC_UART3, UART_INTCFG_RBR, ENABLE);

	// use fifo structure
	UART_FIFO_CFG_Type fifoCfg;
	UART_FIFOConfigStructInit(&fifoCfg);
	UART_FIFOConfig(LPC_UART3, &fifoCfg);

	NVIC_EnableIRQ(UART3_IRQn);
}

/**
 *******************************************************************************************************
 * End XBee UART Stuff *********************************************************************************
 *******************************************************************************************************
 */

Terminal_t * Terminal_new() {
	// returns a singleton instance
	if (termSingleton == NULL) {
		Terminal_t * self = calloc(1, sizeof(Terminal_t));
		self->buffer = calloc(1, UART_BUFF_SIZE);
		termSingleton = self;
	}
    return termSingleton;
}

void Terminal_delete(Terminal_t * self) {
    free(self->buffer);
    if (self->in) Message_delete(self->in);
    if (self->out) Message_delete(self->out);
    free(self);
    termSingleton = NULL;
}

/** Collects information and creates message, if input is ready **/
void Terminal_harvest(Terminal_t * self, uint8_t * buffer, uint32_t length) {

    if (length == 0) return;
    char * offset = (char *) self->buffer + self->buffLen;
    memcpy(offset, buffer, length);
    self->buffLen += length;

    // create new message if end sequence detected
    if (self->buffer[self->buffLen-1] == '\r') {
        if(self->in) {
        	Message_delete(self->in);
        	free(self->in);
        }
        self->in = Message_new(RECIPIENT_FFS, RECIPIENT_BASE, self->buffer);
        self->buffLen = 0;
        // reset buffer 
        if(self->buffer) free(self->buffer);
        self->buffer = calloc(1, UART_BUFF_SIZE);
        // reset time
        Terminal_resetTime(self);
    }
}

/** Checks if there is any incoming message **/
_Bool Terminal_hasMessage(Terminal_t * self) {
    return self->in != NULL;
}

void Terminal_resetTime(Terminal_t * self) {
	self->timeElapsed = SysTick_getMsElapsed();
}

uint32_t Terminal_getTimeElapsed(Terminal_t * self) {
	return SysTick_getMsElapsed() - self->timeElapsed;
}

/** Sends a char string
 *  NOTE: This method auto-appends a newline to end of string.
 * **/
void Terminal_send(Terminal_t * self, const char * payload) {
    if (self->out) {
    	Message_delete(self->out);
    	free(self->out);
    }
    self->out = Message_new(RECIPIENT_BASE, RECIPIENT_FFS, payload);
    char * _payload = Message_get(self->out, true);
    Terminal_resetTime(self);               // reset timeout
    //send payload via uart
    UART_Send(LPC_UART3, (uint8_t *) _payload, strlen(_payload), NONE_BLOCKING);

    // free string
    free(_payload);
    puts("pinging..");
}

/** 
 * Returns message if any, and empties inbox 
 * NOTE: Message pointer must be deleted after use, to minimize occurence of
 * memory leak
 * **/
Message_t * Terminal_retrieve(Terminal_t * self) {
    if (!self->in) return NULL;
    Message_t * result = self->in;
    self->in = NULL;
    return result;
}

