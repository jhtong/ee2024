#include "LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_timer.h"

#include "pca9532.h"
#include "acc.h"
#include "oled.h"
#include "rgb.h"
#include "led7seg.h"
#include "temp.h"
#include "light.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "functions.h"
#include "Vector_Data.h"
#include "FFSClock.h"
#include "FFS_Config.h"

/* Conditionals */
void init_ssp(void) {
	SSP_CFG_Type SSP_ConfigStruct;
	PINSEL_CFG_Type PinCfg;

	/*
	 * Initialize SPI pin connect
	 * P0.7 - SCK;
	 * P0.8 - MISO
	 * P0.9 - MOSI
	 * P2.2 - SSEL - used as GPIO
	 */
	PinCfg.Funcnum = 2;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 0;
	PinCfg.Pinnum = 7;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 8;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 9;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Funcnum = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 2;
	PINSEL_ConfigPin(&PinCfg);

	SSP_ConfigStructInit(&SSP_ConfigStruct);

	// Initialize SSP peripheral with parameter given in structure above
	SSP_Init(LPC_SSP1, &SSP_ConfigStruct);

	// Enable SSP peripheral
	SSP_Cmd(LPC_SSP1, ENABLE);

}

void init_i2c(void) {
	PINSEL_CFG_Type PinCfg;
	/* Initialize I2C2 pin connect */
	PinCfg.Funcnum = 2;
	PinCfg.Pinnum = 10;
	PinCfg.Portnum = 0;
	PINSEL_ConfigPin(&PinCfg);
	PinCfg.Pinnum = 11;
	PINSEL_ConfigPin(&PinCfg);
	// Initialize I2C peripheral
	I2C_Init(LPC_I2C2, 100000);
	/* Enable I2C1 operation */
	I2C_Cmd(LPC_I2C2, ENABLE);
}

/** Helper functions used for calibration mode ****************************************/

void calibration_init(void) {
	oled_clearScreen(OLED_COLOR_BLACK);
	oled_putString(10, 0, (uint8_t *) " CALIBRATION     ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);
	oled_putString(3, 23, (uint8_t *) "    Z-Accel:    ", OLED_COLOR_WHITE,
			OLED_COLOR_BLACK);
	oled_putString(0, 45, (uint8_t *) "        m/s^2  ", OLED_COLOR_WHITE,
			OLED_COLOR_BLACK);

}

void calibration_saveOffset(volatile Vector_Data_t* data) {
	data->zoff = -1 * data->zAccel;
}

void calibration_displayAccel(uint8_t zAcc) {
	float realAcc = (9.81 / GZERO_OFFSET) * zAcc;

	char buffer[3];
	sprintf(buffer, "%.1f", realAcc);
	oled_putString(23, 45, (uint8_t *) buffer, OLED_COLOR_WHITE,
			OLED_COLOR_BLACK);
}

/** End helper functions used for calibration mode ************************************/

/** Helper functions used for standby mode ********************************************/

void standby_countDown(volatile Vector_Data_t* data) {
	if (data->standbyCounter > 0) {
		data->standbyCounter--;
		led7seg_setChar((char) (((int) '0') + data->standbyCounter), FALSE);
	}
}

void standby_init(void) {
	oled_clearScreen(OLED_COLOR_BLACK);
	oled_putString(10, 0, (uint8_t *) "   STANDBY   ", OLED_COLOR_BLACK,
			OLED_COLOR_WHITE);

}

void displayTempLux(volatile Vector_Data_t * data) {
	/* Temperature Sensor */
	float temperature = data->temperature;
	int luxReading = (int) data->light;

	double tempReading = (double) (temperature / 10.0);

	data->isHot = (tempReading > TEMP_MAX);

	char tempBuffer[3];
	sprintf(tempBuffer, "%.1f", tempReading);

	oled_putString(55, 23, (uint8_t *) tempBuffer, OLED_COLOR_WHITE,
			OLED_COLOR_BLACK);
	oled_putString(55 + 14 + 3 + 8, 23, (uint8_t *) "C", OLED_COLOR_WHITE,
			OLED_COLOR_BLACK);

	if (data->isHot) {
		oled_putString(18 - 7 + 1, 23, (uint8_t *) "  HOT ", OLED_COLOR_BLACK,
				OLED_COLOR_WHITE);
	} else {
		oled_putString(13, 23, (uint8_t *) "NORMAL", OLED_COLOR_WHITE,
				OLED_COLOR_BLACK);
	}

	/* Light Sensor */
	data->isRisky = (luxReading >= LIGHT_UPPER_BOUND);

	char luxBuffer[4];

	//TODO: Replace itoa with implemented C function!!
	itoa(luxReading, luxBuffer, 10);

	oled_putString(55, 43, (uint8_t *) luxBuffer, OLED_COLOR_WHITE,
			OLED_COLOR_BLACK);
	oled_putString(55 + (7 * 3), 43, (uint8_t *) "L", OLED_COLOR_WHITE,
			OLED_COLOR_BLACK);

	if (!data->isRisky) {
		oled_putString(10 - 5, 43, (uint8_t *) "  SAFE ", OLED_COLOR_WHITE,
				OLED_COLOR_BLACK);
	} else {
		oled_putString(10 - 5, 43, (uint8_t *) " RISKY ", OLED_COLOR_BLACK,
				OLED_COLOR_WHITE);
	}
}

/** End helper functions used for standby mode ****************************************/

/** Helper functions used for active mode *********************************************/

/**
 * @brief do accelerometer calculations here
 */
void readStoreAccel(volatile Vector_Data_t* data) {
	int8_t xDummy = 0, yDummy = 0;
	acc_read(&xDummy, &yDummy, (int8_t *) &(data->zAccel));
	data->zAccel = data->zAccel + data->zoff;

}

/** For vibrations ------------------------------------------- **/

/**
 * @brief Comparison function, used for quicksort
 * @param a first element
 * @param b second element
 *
 */
int vibrations_cmpFn(const void * a, const void * b) {
	const int _a = *(int8_t *) a;
	const int _b = *(int8_t *) b;
	return _a - _b;
}

/**
 * @brief Returns the smoothed median point, if BUFFER_SIZE is reached.  Otherwise 0.
 * @param rawInput The raw data
 */
int8_t vibrations_getSmoothedMedian(int8_t rawInput) {
	static int8_t buffer[VIBRATION_BUFFER_SIZE], ref;

	if (ref != VIBRATION_BUFFER_SIZE) {
		buffer[ref] = rawInput;
		ref++;
		return 0;
	}

	qsort(buffer, VIBRATION_BUFFER_SIZE, sizeof(int8_t), vibrations_cmpFn);
	int8_t i = (int8_t) ceil(VIBRATION_BUFFER_SIZE / 2);
	int8_t median = buffer[i];
	ref = 0;
	return median;

}

/**
 * @brief Updates vector data struct with the frequency of vibrations.  Place in loop handler.
 */
void vibrations_updateFrequency(volatile Vector_Data_t * data) {
	static uint32_t refMs;
	uint32_t nowMs;
	static int8_t oldVal, newVal;
	static int32_t accumulator;

	if (refMs == 0)
		refMs = SysTick_getMsElapsed();

	nowMs = SysTick_getMsElapsed();

	if ((nowMs - refMs) > 1000) {
		// EXIT and reset variables if reached
		data->vibrationFreq = accumulator;
		refMs = SysTick_getMsElapsed();
		accumulator = 0;
//		return;
	}

	/** do accelerometer calculations here **/

//	printf("%d\n", newVal);
//	printf("%d\n", data->zAccel);
	int8_t curr = vibrations_getSmoothedMedian(data->zAccel);
	if (curr == 0)
		// EXIT IF NO DATA
		return;

	oldVal = newVal;
	newVal = curr;

	// we want opposing signs => a peak => freq++.
	// since number is in 2's Complement, XOR of both will set MSB to 1
	// i.e. number < 0
	if (((newVal ^ oldVal) < 0) && (abs(newVal) > 2)) {
		accumulator++;
	}
}

/** End helper functions used for active mode *****************************************/

void rgbRed_init() {
	GPIO_SetDir(2, 1 << 0, 1);
}

void rgbRed_on() {
	GPIO_SetValue(2, 1 << 0);
}

void rgbRed_off() {
	GPIO_ClearValue(2, 1 << 0);
}

/**
 * @brief Get temp readings every 200 ms
 *
 */
void light_update(volatile Vector_Data_t * data) {
	static uint32_t currMs = 0;
	if (currMs - SysTick_getMsElapsed() > 200) {
		FFSData.light = light_read();
		currMs = SysTick_getMsElapsed();
	}
}

void temperature_update(volatile Vector_Data_t * data) {
	static uint32_t refMs;
	uint32_t nowMs;

	if (refMs == 0)
		refMs = SysTick_getMsElapsed();

	nowMs = SysTick_getMsElapsed();

	// DO NOT READ AND JUST EXIT if not the right time.
	if ((nowMs - refMs) < 5000)
		return;

	// else read temperature
	data->temperature = temp_read();
	refMs = SysTick_getMsElapsed();
	return;
}

void leds_onWarning() {
	pca9532_setBlink0Period(BLINK_INTERVAL);
	pca9532_setBlink0Leds(0xFFFF);
}

void leds_offWarning() {
	pca9532_setLeds(0, 0xFFFF);
}

void TIMER1_IRQHandler() {
	/** Buzzer stuff **/
	static _Bool toggle = 0;
	toggle ? NOTE_PIN_HIGH() : NOTE_PIN_LOW();
	toggle = !toggle;

	/** Clear interrupt **/
	TIM_ClearIntPending(LPC_TIM1, 0);

}

/**
 * @brief turns on buzzer via bit-banging.  External interrupt on TIMER 1
 * allows the generation of a "PWM-like" signal.  This allows the program to
 * NOT pause (and the buzzer to sound continuously) during program execution.
 */
void buzzer_onWarning() {
	/**
	 uint32_t t = 0;
	 //TODO: Write this timing loop in a NON-BLOCKING fashion
	 while(t < 20) {
	 NOTE_PIN_HIGH();
	 Timer0_us_Wait(BUZZER_FREQ / 2);
	 NOTE_PIN_LOW();
	 Timer0_us_Wait(BUZZER_FREQ / 2);
	 t++;
	 }**/

	TIM_TIMERCFG_Type TIM_ConfigStruct;
	TIM_MATCHCFG_Type TIM_MatchConfigStruct;

	// Initialize timer 0, prescale count time of 1ms
	TIM_ConfigStruct.PrescaleOption = TIM_PRESCALE_USVAL;
	TIM_ConfigStruct.PrescaleValue = 1;
	TIM_MatchConfigStruct.MatchChannel = 0;
	TIM_MatchConfigStruct.IntOnMatch = TRUE;
	TIM_MatchConfigStruct.ResetOnMatch = TRUE;
	TIM_MatchConfigStruct.StopOnMatch = FALSE;
	TIM_MatchConfigStruct.ExtMatchOutputType = TIM_EXTMATCH_NOTHING;
	// Set Match value, count value is time (timer * 1000uS =timer mS )
	TIM_MatchConfigStruct.MatchValue = BUZZER_FREQ / 2;

	TIM_Init(LPC_TIM1, TIM_TIMER_MODE, &TIM_ConfigStruct);
	TIM_ConfigMatch(LPC_TIM1, &TIM_MatchConfigStruct);

	// To start timer 0
	TIM_Cmd(LPC_TIM1, ENABLE);
	NVIC_EnableIRQ(TIMER1_IRQn);

}

void buzzer_offWarning() {
	NOTE_PIN_LOW();
	TIM_Cmd(LPC_TIM1, DISABLE);
	TIM_ClearIntPending(LPC_TIM1, 0);
	puts("stopping!");
}

void warning_on() {
	FFSData.warningOn = TRUE;
	buzzer_onWarning();
	leds_onWarning();
	rgbRed_on();
}

void warning_off() {
	FFSData.warningOn = FALSE;
	buzzer_offWarning();
	leds_offWarning();
	rgbRed_off();
}

