/*****************************************************************************
 *   A demo example using several of the peripherals on the base board
 *
 *   Copyright(C) 2011, EE2024
 *   All rights reserved.
 *
 ******************************************************************************/

#include "stdio.h"
#include "LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_timer.h"
#include "lpc17xx_uart.h"

#include "pca9532.h"
#include "acc.h"
#include "oled.h"
#include "rgb.h"
#include "led7seg.h"
#include "temp.h"
#include "light.h"
#include "uart2.h"

#include "FFS_Config.h"

#include "new.h"
#include "new.r"
#include "StateManager.h"
#include "State.h"
#include "ButtonList.h"

#include "FFSClock.h"
#include "Vector_Data.h"
#include "states.h"

#include "functions.h"
#include "SensorLight.h"

#include "statesWireless.h"

/** Using codeRed debug lib **/
#include <stdio.h>


// Handlers for testing ButtonList interrupt class
void btn_reset_handler() {
	puts("button reset was pressed");  // triggered when pressed
}

StateManager_t * wm;
StateManager_t * sm;


int main(void) {
	/** Attach button listeners **/
	// TODO: for now, the interrupt handler does not work.  pass in dummy handler.



	/** custom init functions **/
	init_sysTick();
	init_states();

	/** init protocols **/
	init_i2c();
	init_ssp();

	/** Init peripherals **/
	pca9532_init();
	joystick_init();
	acc_init();
	acc_setMode(ACC_MODE_MEASURE);
	acc_setRange(ACC_RANGE_2G);
	oled_init();
	led7seg_init();
	rgbRed_init();

	GPIO_SetDir(0, 1 << 26, 1);

	/** Init uart, wireless states and wireless manager **/
	init_wirelessStates();
	init_uart();
	wm = StateManager_new();
	StateManager_addState(wm, wState_auth);
	StateManager_addState(wm, wState_active);


	/* Init Light Sensor */
	SensorLight_config();
    /* Init Temperature Sensor */
    temp_init(&SysTick_getMsElapsed); // TODO: Check this typecast

    /* FFS Program Initialization: */
    oled_clearScreen(OLED_COLOR_BLACK);
    led7seg_setChar('5', FALSE);

	/** State Manager stuff **/
    sm = StateManager_new();
    StateManager_addState(sm, state_0);
    StateManager_addState(sm, state_1);
    StateManager_addState(sm, state_2);

	// Replace with interrupt later:
	ButtonList_attachButton(1, 31, btn_reset_handler); 		// CALIBRATE, SW4
	ButtonList_attachButton(2, 10, btn_reset_handler); 		// RESET, SW3

	StateManager_kick(wm); 						// kick wireless manager
    StateManager_kick(sm); 						// kick the SM

    // run managers
    while(1) {
		StateManager_tick(wm);
    	StateManager_tick(sm);
    }
}

void check_failed(uint8_t *file, uint32_t line) {
	/* User can add his own implementation to report the file name and line number,
	 ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

	/* Infinite loop */
	while (1)
		;
}

