#include "states.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "FFS_Config.h"
#include "State.h"
#include "StateManager.h"
#include "Vector_Data.h"
#include  "ButtonList.h"

#include "FFSClock.h"

#include "temp.h"
#include "light.h"

#include "functions.h"

extern StateManager_t * sm;

/** Auxiliary functions ------------------------------------------------------------------------------------**/

/** Updates sensor data via polling.  This is called on every tick **/
static void update_sensorData() {

	FFSData.btn_calibrate = ButtonList_getButtonState(1, 31);

	if (sm->refNow == 0 || sm->refNow == 2) { // Read accelerometer only if in CALIBRATE OR ACTIVE
		readStoreAccel(&FFSData);
		vibrations_updateFrequency(&FFSData);
	}

	if (sm->refNow == 1 || sm->refNow == 2) { // Read temp and light only if in STANDBY OR ACTIVE
		// execute every 200ms
		static uint32_t currMs = 0;
		if (currMs - SysTick_getMsElapsed() > 500) {
			FFSData.light = light_read();
			temperature_update(&FFSData);
			currMs = SysTick_getMsElapsed();
		}
	}

}

/** Since interrupts introduce need for mutex handling,
 * we avoid need for mutexes by checking against value
 */
static void clearLightFlag() {
	if (FFSData.isHot) {
		uint32_t lightVal = FFSData.light;
		if ((LIGHT_LOWER_BOUND < lightVal)
			&& (lightVal < LIGHT_UPPER_BOUND)) {
				FFSData.isHot = 0;
				SysTick_delay(1);
		}
	}
}

/** Clear flags triggered by interrupt at end of tick **/
static void clearFlags() {
	clearLightFlag();
}

/** External interrupt handling for RESET button **/
void EINT0_IRQHandler(void) {
	LPC_SC->EXTINT = 0x01; // Clear the EXTINT register bit 0 (EINT0) interrupt flag (p. 25)
	NVIC_ClearPendingIRQ(EINT0_IRQn);
	StateManager_goto(sm, 0); // signal SM to do mem clearing and enter CALIBRATION upon next tick
}

/** End auxiliary functions --------------------------------------------------------------------------------**/

/***** State CALIBRATION *****/
void tickHandler0() {
	update_sensorData();
	calibration_displayAccel(FFSData.zAccel);

	if (FFSData.btn_calibrate) {
		StateManager_goto(sm, 1);
	}

	clearFlags();
}
void enterHandler0() {
	FFSData.zoff = 0;

	calibration_init();
	led7seg_setChar('5', FALSE);

	warning_off();
}
void endHandler0() {
	calibration_saveOffset(&FFSData);
	FFSData.isHot = 0;
	FFSData.isRisky = 0;
}

/***** State STANDBY *****/
void tickHandler1() {
	update_sensorData();

	// reset back to 5 if hot
	if (FFSData.isHot)
		FFSData.standbyCounter = 5;

	if (FFSData.standbyCounter == 0 && !FFSData.isRisky && !FFSData.isHot) {
		StateManager_goto(sm, 2);
		return;
	}

	// Decrement counter for the first 5 sec:
	static uint32_t prevMs = 0;

	if ((SysTick_getMsElapsed() - prevMs) > 1000) {
		prevMs = SysTick_getMsElapsed();
		standby_countDown(&FFSData);
	}

	displayTempLux(&FFSData);
	clearFlags();
}

void enterHandler1() {
	FFSData.standbyCounter = 5;
	standby_init();
	warning_off();
}

void endHandler1() {

}

/***** State ACTIVE *****/
void tickHandler2() {
	update_sensorData();

	// go back to COUNTDOWN state if hot or risky
	if (FFSData.isHot || FFSData.isRisky) {
		StateManager_goto(sm, 1);
		return;
	}

	displayTempLux(&FFSData);

	// TODO: Get rid of polling below -------------------------------------------------

	/** Warn if frequency is too high **/
	int32_t freq = FFSData.vibrationFreq;
	if ((FREQ_UNSAFE_LOWER <= freq) && (freq < FREQ_UNSAFE_UPPER)) {
		if (!FFSData.warningOn)
			warning_on();
	} else {
		if (FFSData.warningOn)
			warning_off();
	}

	clearFlags();
}

void enterHandler2() {
	active_init();
}

void endHandler2() {
}

/** Public functions --------------------------------------------------- **/
void init_states() {
	state_0 = State_new(tickHandler0, enterHandler0, endHandler0);
	state_1 = State_new(tickHandler1, enterHandler1, endHandler1);
	state_2 = State_new(tickHandler2, enterHandler2, endHandler2);
}

void free_states() {
	State_delete(state_0);
	State_delete(state_1);
	State_delete(state_2);
}
/** End Public functions ----------------------------------------------- **/
