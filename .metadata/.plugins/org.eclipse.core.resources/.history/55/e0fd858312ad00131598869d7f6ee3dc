#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "StateManager.h"
#include "State.h"
#include "new.h"
#include "new.r"

static void * StateManager_ctor(void * _self, va_list * app) {
	struct StateManager * self = _self;
	self->numStates = 0;
	/** Begin alloc internal vars here **/
	/**                                **/
	/** End alloc internal vars here   **/
	return self;
}

static void * StateManager_dtor(void * _self) {
	struct StateManager * self = _self;
	/** Begin Free internal vars here **/

	unsigned int i;

	// first run exit function of exit state (to perhaps clear memory)
	StateManager_getState(self->stateNow)->on_exit();

	// delete all states
	for (i = 0; i < self->numStates; i++) {
		delete(self->states[i]);
	}

	/** End Free internal vars here   **/
	return self;
}

static void * StateManager_clone(const void * _self) {
	// is like a singleton, should not be cloned
	return NULL ;
}

static int StateManager_differ(const void * _self, const void * _b) {
	// is like a singleton, should not be differed
	return 0;
}

static const struct Class _StateManager = { sizeof(struct State),
		StateManager_ctor, StateManager_dtor, StateManager_clone,
		StateManager_differ };

/** Expose class **/
const void * StateManager = &_StateManager;

/***********************************************/
/****   Public custom functions go here     ****/
/***********************************************/

// to be accessed only by utility functions
StateManager_t * sm = NULL;

StateManager_t * StateManager_getInstance() {
	if (sm)
		return sm;
	sm = new(StateManager);
	return sm;
}

_Bool StateManager_stateExists(State_t * state) {
	unsigned int i = 0;
	StateManager_t * _self = StateManager_getInstance();

//	assert(state);

	State_t * a = state, * b;
	for (i = 0; i < _self->numStates; i++) {
		b = (State_t *) _self->states[i];
		if (!differ(a, b))
			return 1;
	}
	return 0;
}

State_t * StateManager_getState(const uint32_t stateNumber) {
	StateManager_t * _self = StateManager_getInstance();

//	assert(stateName);

	//assert(stateNumber >= -1);
	return _self->states[stateNumber];
}

/** States are not removable once added 
 * to State Manager (until destruction) 
 **/
_Bool StateManager_addState(State_t * state) {
	StateManager_t * _self = StateManager_getInstance();
//	assert(state);

	if (StateManager_stateExists(state))
		return 0;

	if (_self->numStates == MAX_STATES)
		return 0;

	uint32_t a = (uint32_t) _self->numStates;
	sm->states[a] = state;
	sm->numStates++;
	return 1;
}

void StateManager_goto(const uint32_t stateNumber) {
	StateManager_t * _self = StateManager_getInstance();
	//assert(stateNumber > -1 && stateNumber < _self->numStates);
	_self->stateNext = stateNumber;
}

/** Default state is the first state loaded **/
void StateManager_begin() {
	StateManager_t * _self = StateManager_getInstance();
	//TODO THERE IS A BUG HERE with string copying
	_self->stateNext = 0;
	_self->stateNow = 0;
	State_t * initialState = StateManager_getState(_self->stateNow);
	initialState->on_enter();

	// run manager until power off
	while(1)
        StateManager_tick();
}

void StateManager_tick() {
	StateManager_t * _self = StateManager_getInstance();
	uint32_t now = _self->stateNow;
	uint32_t next = _self->stateNext;
	State_t * nowState = StateManager_getState(now);
	State_t * nextState = StateManager_getState(next);

	if (now != next) {
		nowState->on_exit();
		nextState->on_enter();
	}

	_self->stateNow = _self->stateNext;
	// needed as may have changed
	nowState = StateManager_getState(_self->stateNow);
	nowState->on_tick();
}



