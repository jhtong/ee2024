# EE2024 Assignment 2

| Name | Matric Number |
| -- | -- |
| Joel Haowen **Tong** | A0108165J |
| Mohit **Shridhar** | A0105912N |

**Lab Group: Wednesday 9 PM **
** Graduate Assistant: **


##Introduction


##Design


## UML Diagrams

one UML use case diagram and one UML Statechart

## Implementation

include problems you encountered and how you solved them for these sections:

Round robin architecture

### Sensor Data

An exclusive data structure `Vector_Data_t` was created to store all the readings essential for the core functionality of the system. This allowed us to keep the data more organized and paved the way for a more systematic approach towards polling the sensor readings – `update_sensorData()`. 

| Name | Data Type | Use State |
| -- | -- |
| btn_calibrate | _Bool | Calibration |
| zAccel | int8_t | Calibration & Active |
| zOff | int32_t | Calibration & Active |
| standbyCounter | int32_t | Standby |
| previousMs | uint32_t | Standby |
| delayMs | uint32_t | Standby
| light | uint32_t | Standby & Active |
| temperature | float | Standby & Active |
| isHot | _Bool | Standby & Active |
| isRisky | _Bool | Standby & Active |
| vibrationFreq | int32_t | Active |
| warningOn | _Bool | Active |
| msTicks | uint32_t | All States |


### Calibration

**Peripherals Used:** Acclerometer, OLED Display

**Enter Handler:** 

	Reset zOff to zero
    Initialize static elements on the OLED display for Active state
    Reset 7-Seg display to '5'
    Turn off FFS warning

**Exit Handler:** 

	Save zOff value 
    Initialize isHot & isRisky booleans to false

##### Accelerometer Readings

A standard I2C protocol was used to read off the accelerometer values every time `update_sensorData()` was called. The sensor was polled only during Calibration & Active state to reduce wasted clock cycles in the round-robin loop. After polling, the new value was offset using `zOff` and then stored in `zAccel`. The offset has no effect in Calibration mode as `zOff` is yet to be intialized. However, the offset will be used later in Active mode to zero the z-axis reading.

##### OLED Display

7 seg init

##### Reset Interrupt (SW3)

### Standby

**Peripherals Used:** Temperature & Light Sensor, OLED Display, 7-Seg Display

`JOEL PLEASE CHECK THIS THIS!!!!!!!!!!`
**Enter Handler:** 

	Reset standbyCounter to 5
    Initialize static elements on OLED Display for Standby state
    Turn off FFS warning
	Initiate UART Handshake

##### 7-Seg Counter

##### Temperature and Light Sensor

##### Calibrate Button (SW4)

##### UART Handshake

### Active

**Peripherals Used:** Acclerometer, Temperature & Light Sensor, OLED Display, LED Array, RGB LED, Buzzer, UART Interface

`JOEL PLEASE CHECK THIS THIS!!!!!!!!!!`
**Enter Handler:**

	Initialize static elements on OLED Display for Active state

##### Safe Conditions

##### Flutter Frequency Analysis

###### Algorithm

###### Noise Reduction

##### Warning Indicators

##### UART Interface

## Extensions

### State Manager

### XBee Wireless


## Limitations & Improvements


## Conclusion

## Statement of Contribution

Student 1: A0108165J
Student 2: A0105912N

**A. Joint Work in algorithm development, programming and report writing (briefly list a few specific aspects):**
...
**B. Student 1’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):**
...
**C. Student 2’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):**
...
We both agree that the statements above are truthful and accurate.


Signatures and Names:

