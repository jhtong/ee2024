<!-- Template for App View -->

<script type="text/javascript"
  src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$$$','$$$'], ['\\(','\\)']]}});
</script>
<h1>EE2024 Assignment 1</h1>
<table>
<thead>
<tr>
<th>Name</th>
<th>Matric Number</th>
</tr>
</thead>
<tbody>
<tr>
<td>Joel Haowen <strong>Tong</strong></td>
<td>A0108165J</td>
</tr>
<tr>
<td>Mohit <strong>Shridhar</strong></td>
<td>A0105912N</td>
</tr>
</tbody>
</table>
<h2>Objective</h2>
<p>This report explores the Gradient Descent algorithm to find the minimum for the polynomial <span class="mathjax">$$$ax^2 + bx + c$$$</span>, using both ARM assembly and C.</p>
<h3>Background</h3>
<p>The Gradient Descent algorithm finds the minimum of a given function.  It has a complexity of O(n).</p>
<p>If a given function <span class="mathjax">$$$F(x)$$$</span> is well-defined and differentiable within a neighborhood that encloses the minimum point <span class="mathjax">$$$x_{min}$$$</span> and <span class="mathjax">$$$x_0$$$</span>, then one should travel in the direction opposite the gradeint to reach the minimum point <span class="mathjax">$$$x_{min}$$$</span>, specifically:</p>
<p class="mathjax">$$
F(x_{k+1}) = F(x_k) - \lambda \nabla F(x) : k \in \mathbb{N}$$</p><p>For sufficiently small values of <span class="mathjax">$$$\lambda < 1$$$</span>, </p>
<p class="mathjax">$$
F(x_{k+1}) \leq F(x)$$</p><p>The local minimum is given by the terminating condition:</p>
<p class="mathjax">$$
F(x_{n+1}) \approx F(x_n) : \exists \, n : x_{n+1} = x_{min} \bullet n  \in \mathbb{N}$$</p><p>An example of the algorithm in operation is given by:</p>
<p><img src="http://upload.wikimedia.org/wikipedia/commons/6/60/Banana-SteepDesc.gif" style="width: 300px;"  /></p>
<h2>Code</h2>
<h3>The General Idea</h3>
<p>Our gradient descent algorithm in Assembly evaluates the following two expressions:</p>
<p class="mathjax">$$
\begin{align}
\nabla f (x_k) &= 2ax_k + b \\
x_{k+1} &= x_k - \lambda \nabla f(x_k)
\end{align}$$</p><p>The loop guard for the routine is given by:</p>
<p class="mathjax">$$
x_{k+1} = x_k$$</p><p>From this, we can derive our algorithm as follow, in pseudo-code:</p>
<pre><code>optimize:
    1. Store the context
    2. Store x_k
    3. Evaluate Expression 1
    4. Evaluate Expression 2
    5. Jump to &lt;optimize&gt; if x_k !== x_k+1.

found:
    1. Write over x_k with x_k+1
    2. Restore the context
    3. Exit
</code></pre><h3>Details</h3>
<h4>Scaling</h4>
<p>As both <span class="mathjax">$$$x_k$$$</span> and <span class="mathjax">$$$\lambda$$$</span> are given as floating-point numbers, we scale the expressions to the scale of <code>100</code> for expression (1), and <code>1000</code> for expression (2) respectively to preserve precision.  This scaling is necessary, as the ARMv7 instruction set does not come with floating-point support out-of-box.  The expressions are hence:</p>
<p class="mathjax">$$
\begin{align}
100 ( \nabla f (x_k) ) &= 100( 2ax_k + b ) \\
1000 (x_{k+1}) &= 1000(x_k - \lambda \nabla f(x_k))
\end{align}$$</p><p>To ensure consistency in scaling, input was first converted to an integer-based version by multiplying the float input with <code>100</code>.  Output from <code>optimize()</code> was then rescaled back and typecasted to a float.  Care was taken to preserve scaling between the two expressions.  The following properties were used to preserve scaling:</p>
<h5>Distributive</h5>
<p class="mathjax">$$
c (A + B) = cA + cB$$</p><h5>Associative</h5>
<p class="mathjax">$$
cd (AB) = c A \cdot dB$$</p><h3>The Assembly</h3>
<h4>Register Map</h4>
<p>The outlay of general-purpose registers is given in the map below, and was used to ensure neat code and minimal use of registers.</p>
<table>
<thead>
<tr>
<th>r0</th>
<th>r1</th>
<th>r2</th>
<th>r3</th>
<th>r4</th>
<th>r5</th>
<th>r6</th>
</tr>
</thead>
<tbody>
<tr>
<td><span class="mathjax">$$$x_i$$$</span></td>
<td><span class="mathjax">$$$a$$$</span></td>
<td><span class="mathjax">$$$b$$$</span></td>
<td><span class="mathjax">$$$c$$$</span></td>
<td><span class="mathjax">$$$\lambda$$$</span></td>
<td>misc.</td>
<td>misc.</td>
</tr>
</tbody>
</table>
<p><br></p>
<table>
<thead>
<tr>
<th>r7</th>
<th>r8</th>
<th>r9</th>
<th>r10</th>
<th>r11</th>
<th>r12</th>
</tr>
</thead>
<tbody>
<tr>
<td>misc.</td>
<td>misc.</td>
<td>misc.</td>
<td>misc.</td>
<td><span class="mathjax">$$$i$$$</span></td>
<td>count_add</td>
</tr>
</tbody>
</table>
<h4>Defining constants</h4>
<p>Constants were defined by using the <code>.word</code> and <code>.equ</code> directives.  The difference between the directives is that <code>.equ</code> defines a constant, whereas in the case of <code>.word</code>, space is allocated on the stack for the constant and initialized.</p>
<pre><code>@ Equates
    .equ    STEP,        2                    @lambda at x10
    .equ    SCALE,      10
    .equ    SCALE2,    100
    .equ    LCOUNT,    0

@ Define constant values here if necessary
    COUNT_ADD: .word COUNT
</code></pre><p>They are then loaded upon entering the routine:</p>
<pre><code>    @ load constants
    ldr     r11, COUNT_ADD           @ global variable COUNT reference
    ldr     r10, =LCOUNT             @ initalize counting register
    ldr     r8, =SCALE               @ Store 10 in R8
    ldr     r9, =SCALE2              @ Store 100 in R9
</code></pre><h4>Performing normal subroutine operations</h4>
<p>A routine written in Assembly does not preserve the program’s context by default.  The state of the registers (context), and the link register (containing the reference address just before the point of entry into the subroutine) needs to be saved prior to modification of the registers.  Failure to do so might result in a runtime error, for example a hard fault.  As such, we preserve the context by pushing it onto the heap, and pop it from the heap just before returning from the routine:</p>
<pre><code>push     {r1-r12,lr}             @ save the context
</code></pre><h4>Computing <span class="mathjax">$$$\nabla f(x)$$$</span></h4>
<p>Computation of the derivative is given as follow.  Care was taken to preserve scaling at x100.</p>
<pre><code>    @ 2ax + b at x100
    lsl     r5, r0, #1               @ (xi*100) * 2, Store in R5
    mul     r7, r2, r9               @ b * 100, Store in R7
    mla     r6, r1, r5, r7           @ a * [(xi*100) * 2] + [b * 100], Store in R6
</code></pre><p>A logical left shift <code>LSL</code> operation was used to multiply by 2, while <code>MLA</code> enabled two operations (multiplication and subsequently addition) to be performed, reducing the number of instructions written.  While it is noted that to fully accomodate 32-bit multiplication would require a 2 words (64-bit) space, a 32-bit space was assumed as subsequent iterations would require even larger spaces (e.g. 64-bit multiplication in iteration 2, 4 words).  Hence the upper 32-bits were discarded.</p>
<h4>Computing the second expression</h4>
<p><span class="mathjax">$$$x_{k+1}$$$</span> was computed in the second section.  As the formula involved <span class="mathjax">$$$\lambda : \lambda = 0.2 \Rightarrow \lambda \leq 1$$$</span>, and <span class="mathjax">$$$\lambda$$$</span> (given by step) had to be scaled up by 10.  Hence, the total scaling was x1000.  Care was taken to preserve scaling at that value.  </p>
<p>The finalized version of <span class="mathjax">$$$x_{k+1}$$$</span> was then renormalized to x100 by performing an <code>sdiv</code> operation.</p>
<pre><code>    @ x_k+1 at x1000
    ldr     r4, =STEP                @ Store STEP * 10 in R4
    mul     r6, r6, r4               @ {a * [(xi*100) * 2] + [b * 100]} * (STEP*10), Store in R6

    mul     r7, r0, r8               @ (xi*100) * 10, Store in R7
    sub     r5, r7, r6               @ |(xi*100) * 10| – |{a * [(xi*100) * 2] + [b * 100]} * (STEP*10)|, Store in R5

    @ x_k+1 at x100
    sdiv    r6, r5, r8               @ \\\|(xi*100) * 10| – |{a * [(xi*100) * 2] + [b * 100]} * (STEP*10)|\\\ ÷ 10, Store in R6
                                     @ restore x_k+1 scaling to x100
</code></pre><h4>Comparison until result is reached</h4>
<p>The <span class="mathjax">$$$x_k$$$</span> and <span class="mathjax">$$$x_{k+1}$$$</span> were then shifted into temporary locations.  <span class="mathjax">$$$x_{k+1}$$$</span> was first shifted into <code>r0</code>, the return register, in anticipation of the event the loop guard was satisfied and exited.  The loop guard, <span class="mathjax">$$$x_{k+1} = x_k$$$</span>, was computed by doing a <code>bne</code> operation which triggered the status register <code>Z</code> flag, and subsequently a <code>bne</code> which would branch if they were not equal (i.e. the Z flag was not raised).  Iteration would continue until the loop condition was satisfied.</p>
<pre><code>compare:
    mov     r5, r0                   @ moving for temp comparison
    mov     r0, r6                   @ moving so it can loop back or return val, if needed
    cmp     r6, r5                   @ subtract and trigger sReg if zero
    bne     loop                     @ loop if not equal (Z flag checked) with r0 = x_k+1
</code></pre><h4>Exiting</h4>
<p>The number of iterations, stored in <code>r10</code>, was then written to the address pointed by <code>r11</code>, i.e. the global <code>COUNT</code>.  Context was then restored by popping the pushed registers on the stack, and doing an implicit branch to the return address of the link register given by the contents of the link register <code>lr</code>.  Note that in particular, the address of the link register (prior to subroutine entry) on the stack is instead pushed to the program counter, resuming execution.  Hence a <code>bx lr</code> instruction is not needed.</p>
<pre><code>    str     r10, [r11]               @ write global varible COUNT

    pop     {r1-r12, pc}             @ restore the context
</code></pre><h2>Experiments &amp; Discussion</h2>
<h4>λ Variation</h4>
<p><strong>Examine the effect of changing the step size parameter λ on the number of iterations needed
to converge to the optimal value x* and the accuracy of the solution.
Note: this can be seen from the C program console window output</strong></p>
<table>
<thead>
<tr>
<th>λ = 0.1 Full</th>
<th>λ = 0.5 Full</th>
<th>λ = 0.9 Full</th>
</tr>
</thead>
<tbody>
<tr>
<td><img src="http://joeltong.org/ee2024-lab-1/λ=0.1_Full.pdf.jpeg" style="width: 300px;"></td>
<td><img src="http://joeltong.org/ee2024-lab-1/λ=0.5_Full.pdf.jpeg" style="width: 300px;"></td>
<td><img src="http://joeltong.org/ee2024-lab-1/λ=0.9_Full.pdf.jpeg" style="width: 300px;"></td>
</tr>
</tbody>
</table>
<p>It is apparent from the graphs above that there clearly exists a non-linear relationship between the step size <span class="mathjax">$$$(λ)$$$</span> and the number of iterations <span class="mathjax">$$$(n)$$$</span> executed. For all parameter values of the quadratic function <span class="mathjax">$$$ax^2 +bx+c$$$</span>, there exists <span class="mathjax">$$$λ_{max}$$$</span>, such that the gradient decent algorithm never reaches a stable <span class="mathjax">$$$x_{min}$$$</span> value due to the hinderance of physical computational limits. When <span class="mathjax">$$$λ = 0.1$$$</span>, as in the case above, the algorithm smoothly descends into the correct minimum value. As <span class="mathjax">$$$λ$$$</span> is increased up until <span class="mathjax">$$$0.5$$$</span>, the number of iterations decrease because the ‘changes’ between <span class="mathjax">$$$x_{k}$$$</span> and <span class="mathjax">$$$x_{k+1}$$$</span> get larger and closer to the minimum value. But after <span class="mathjax">$$$λ=0.5$$$</span>, the ‘changes’ are so substantial that they sway back and forth between the two halves of the function. However, it is important to note that this is an isolated case with fixed parameters <span class="mathjax">$$$a, b, c$$$</span>. Different parameters will result in different ‘pivots’ like <span class="mathjax">$$$λ=0.5$$$</span>. </p>
<h4>ASM vs. C: Iteration</h4>
<p><strong>Compare the number of iterations taken by the assembly language function with that of the
C program and explain your observations.
Note: this part is to be done without any assistance from your GA or tutor.</strong></p>
<p><img src="http://joeltong.org/ee2024-lab-1/Step_vs._Iterations.pdf.jpeg" style="width: 600px;"></p>
<p>In most cases, the number of iterations performed by <code>optimize()</code> is smaller than that of the <code>main()</code> program. This is clearly evident in the case above for <span class="mathjax">$$$0.1 ≤ λ ≤ 0.9$$$</span>. The effect is mainly a result of the difference in computational precision between the ASM and the C program. During the execution of numerical operations, <code>main()</code> is designed to handle floating point values (single precision: 32-bit). On the other hand, <code>optimize()</code> can only compute with scaled interger values, and thus has a limited ability to process smaller iterations between <span class="mathjax">$$$x_{min}$$$</span> and <span class="mathjax">$$$x_{0}$$$</span>. Despite the differences in precision, the ‘pivot’ at <span class="mathjax">$$$λ=0.5$$$</span> is still apparent in the C program due to the same reasons stated in the previous section. </p>
<h4>ASM vs. C: Accuracy</h4>
<p><strong>Note: the solution for x which can be achieved using the assembly language
optimize() function may not be as precise as that which can be achieved through
gradient descent implemented in the C program. Compare these two solutions with the
true solution obtained through mathematical analysis in your report and discuss why
they are the same or different</strong></p>
<p><img src="http://joeltong.org/ee2024-lab-1/ASM_Error_vs._C_Error.pdf.jpeg" style="width: 600px;"></p>
<p>The accuracy of all the solutions computed in the previous section was compared with the theoretically perfect <span class="mathjax">$$$x_{min}$$$</span> value, i.e. <span class="mathjax">$$$\frac{-b}{2a}$$$</span>. An interesting outcome of this experiment: higher number of iterations doesn’t directly correlate to greater accuracy. In the case <span class="mathjax">$$$a=1, b=-14, c=45$$$</span>, the ASM solution is 100% accurate for all step sizes; the C program has negligibly small and yet non-zero errors. This is an isolated case where <span class="mathjax">$$$x_{min}$$$</span> happens to be a whole number. If <span class="mathjax">$$$a=4, b=-1, c=30$$$</span>, where <span class="mathjax">$$$x_{min}$$$</span> is a fraction, then the ASM solution is defective with 4% error, whereas the C solution has effectively 0% error. Generally, <code>optimize()</code> performs worse when <span class="mathjax">$$$x_{min}$$$</span> is a fractional value or a number with many decimal places, due to the fact that the scaling function limits its decimal precision to only two places. When <span class="mathjax">$$$x_{min}$$$</span> is a whole number, the accuracy of the individual decimal places doesn’t matter, and often results in 0% error. </p>
<h3>Limitations:</h3>
<h4>Algorithm</h4>
<p>The gradient descent algorithm itself has a fundamental physical limitation. For both ASM and C implementations, there exist <span class="mathjax">$$$λ_{a,max}$$$</span> and <span class="mathjax">$$$λ_{c,max}$$$</span> respectively, for which step sizes beyond these values will result in an infinite loop when executed on the ARM processor. This is either because the ‘change’ between <span class="mathjax">$$$x_{k}$$$</span> and <span class="mathjax">$$$x_{k+1}$$$</span> during each iteration is so violent that a 32 bit register can no longer handle the value. Or as in the case <span class="mathjax">$$$a=7, b=-160, c=21, λ=0.2$$$</span>, the ASM program will be consumed by a state of instability, toggling between <span class="mathjax">$$$11.42$$$</span> and <span class="mathjax">$$$11.43$$$</span> for infinite iterations (decimal place accuracy limitation). Theoretically, if we were in the possession of a computer capable of infinite processing power, then it should be possible to calculate <span class="mathjax">$$$x_{min}$$$</span> with 100% accuracy for any <span class="mathjax">$$$a,b,c, λ$$$</span> in both ASM and C.</p>
<h4>32-bit Multiplication</h4>
<p>While computing <span class="mathjax">$$$\lambda \nabla f(x_k)$$$</span>, the multiplication of 32-bit numbers lead to 64-bit results. The effects of this problem are negligible for results which constitute of 32-bits or less. Anything greater than 32-bits will be truncated, thus limiting the range of values for which the algorithm can perform properly.</p>
<h4>ASM <span class="mathjax">$$$x_{0}$$$</span> precision</h4>
<p>The algorithm’s scaling function can only take in <span class="mathjax">$$$x_{0}$$$</span> values with a maximum of 2 decimal points. Any decimal places after that will be truncated and ignored by <code>optimize()</code>. <em>Note:</em> The decimal precision of the ASM function can be increased by changing the scaling values. For example, instead of scaling the algorithm by 100, if we scale it by 1000, the solution <span class="mathjax">$$$x_{min}$$$</span> will be evaluated to one more decimal place. </p>
<h2>Statement of Contribution</h2>
<table>
<thead>
<tr>
<th>Student 1</th>
<th>Student 2:</th>
</tr>
</thead>
<tbody>
<tr>
<td>Joel Haowen <strong>Tong</strong> (A0108165J)</td>
<td>Mohit <strong>Shridhar</strong> (A0105912N)</td>
</tr>
</tbody>
</table>
<p><strong>A. Joint Work in algorithm development, programming and report writing (briefly list a few specific aspects):</strong></p>
<p>Main algorithm execution/development, ASM debugging, experimentation</p>
<p><strong>B. Student 1’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):</strong></p>
<p>Main algorithm design, ASM &amp; C debugging, Report: Background, Code</p>
<p><strong>C. Student 2’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):</strong></p>
<p>Bonus question, supplementary functions in C, report: Experiments &amp; Discussion</p>
<p><strong>We both agree that the statements above are truthful and accurate.</strong></p>
<p><strong>Signatures and Names:</strong></p>
<p><br></p>
<table>
<thead>
<tr>
<th>Joel Haowen <strong>Tong</strong> (A0108165J)</th>
<th>Mohit <strong>Shridhar</strong> (A0105912N)</th>
</tr>
</thead>
<tbody>
<tr>
<td><br><br><br></td>
<td><br><br><br></td>
</tr>
</tbody>
</table>
<h2>Appendix 0: The Code</h2>
<p><strong>main.c</strong></p>
<pre><code>/** main.c **/

#include &quot;stdio.h&quot;
#include &quot;math.h&quot;

// EE2024 Assignment 1

/**
    @ Authors:
    @ TONG Haowen Joel &lt;me at joeltong dot org&gt;
    @ SHRIDHAR Mohit &lt;mohit at nus dot edu dot sg&gt;
*/

// Optimization routine written in assembly language
// Input parameters (signed integers):
//     xi : integer version of x
//     a, b, c : integer coefficients of f(x)
// Output : returns solution x (signed integer)

unsigned int COUNT; // global variable passed to ASM
extern int optimize(int xi, int a, int b, int c);


int main(void)
{
    int a=1, b=-14, c=45, xsoli; // Default a, b, c
    float fp, x, xprev, xsol, change, lambda=0.2, perfect;

    printf(&quot;Enter the parameters of the quadratic function (a[x^2] + b[x] + c) to be optimized\n&quot;);

    printf(&quot;Int a: &quot;);
    scanf(&quot;%d&quot;, &amp;a);

    printf(&quot;Int b: &quot;);
    scanf(&quot;%d&quot;, &amp;b);

    printf(&quot;Int c: &quot;);
    scanf(&quot;%d&quot;, &amp;c);

    printf(&quot;Initial value of x (press [Enter] after keying in): &quot;); // eg: try x0 = -12.35
    scanf(&quot;%f&quot;, &amp;x);

//  ARM ASM &amp; Integer version

    printf(&quot;ARM ASM &amp; Integer version:\n&quot;);
    xsoli = (int) optimize((int) (x * 100),a,b,c);
    xsol = ( (float) xsoli ) / (100.0);
    printf(&quot;Iterative ASM solution : %f (%d iterations)\n&quot;,xsol, COUNT);

    perfect = (b*-1.0)/(2.0*a);
    printf(&quot;Theoretical solution: %f\n\n&quot;, perfect);

//  C &amp; Floating Point version

    printf(&quot;C &amp; Floating point version:\n&quot;,x);
    int countC = 0;
    while (1)
    {
        countC++;

        fp = 2*a*x + b;

        xprev = x;
        change = -lambda*fp;
        x += change;

        printf(&quot;x: %f, fp: %f, change: %f\n&quot;, x, fp, change);
        if (x==xprev) {
            printf(&quot;Floating point C Iteration: %f\n&quot;, x);
            break;
        }
    }

    printf(&quot;\nResult: ASM: %f (%d iterations), C floating: %f (%d iterations), Theoretical: %f\n&quot;, xsol, COUNT, x, countC, perfect);

    // Calculate error:
    float errorAsm = (fabs(perfect-xsol) / (perfect * 1.0)) * 100.0;
    float errorC = (fabs(perfect-x) / (perfect * 1.0)) * 100.0;
    printf(&quot;ASM Error: %f%%, C-floating Error: %f%%\n&quot;, errorAsm, errorC);

    // Enter an infinite loop, just incrementing a counter
    // This is for convenience to allow registers, variables and memory locations to be inspected at the end
    volatile static int loop = 0;
    while (1) {
        loop++;
    }
}
</code></pre><p><strong>optimize.s</strong></p>
<pre><code>     .syntax unified
     .cpu cortex-m3
     .thumb
     .align 2
     .global    optimize
     .global COUNT                       @ global variable
     .thumb_func


@ EE2024 Assignment 1
@ Authors:
@ TONG Haowen Joel &lt;me at joeltong dot org&gt;
@ SHRIDHAR Mohit &lt;mohit at nus dot edu dot sg&gt;

@ Equates
    .equ     STEP,      2                @lambda at x10
    .equ     SCALE,     10
    .equ     SCALE2,    100
    .equ     LCOUNT,    0


optimize:
    @ TODO: THIS VERSION DOES NOT CONSIDER EXCEPTIONS

    @ General Purpose Register Map
    @ r0:         [xk*100], return value
    @ r1:         a
    @ r2:         b
    @ r3:         c

    @ r8:         SCALE 10
    @ r9:         SCALE 100


    preamble:
        push     {r1-r12,lr}             @ save the context

        @ load constants
        ldr     r11, COUNT_ADD           @ global variable COUNT reference
        ldr     r10, =LCOUNT             @ initalize counting register
        ldr     r8, =SCALE               @ Store 10 in R8
        ldr     r9, =SCALE2              @ Store 100 in R9

    loop:
        add     r10, r10, #1             @ increment counter

        @ 2ax + b at x100
        lsl     r5, r0, #1               @ (xk*100) * 2, Store in R5
        mul     r7, r2, r9               @ b * 100, Store in R7
        mla     r5, r1, r5, r7           @ a * [(xk*100) * 2] + [b * 100], Store in R5

        @ x_k+1 at x1000
        ldr     r4, =STEP                @ Store STEP * 10 in R4
        mul     r5, r5, r4               @ {a * [(xk*100) * 2] + [b * 100]} * (STEP*10), Store in R6

        mul     r6, r0, r8               @ (xk*100) * 10, Store in R7
        sub     r5, r6, r5               @ |(xk*100) * 10| – |{a * [(xk*100) * 2] + [b * 100]} * (STEP*10)|, Store in R5

        @ x_k+1 at x100
        sdiv    r6, r5, r8               @ \\\|(xk*100) * 10| – |{a * [(xk*100) * 2] + [b * 100]} * (STEP*10)|\\\ ÷ 10, Store in R6
                                         @ restore x_k+1 scaling to x100

    compare:
        mov     r5, r0                   @ moving for temp comparison
        mov     r0, r6                   @ moving so it can loop back or return val, if needed
        cmp     r6, r5                   @ subtract and trigger sReg if zero
        bne     loop                     @ loop if not equal (Z flag checked) with r0 = x_k+1

        str     r10, [r11]               @ write global varible COUNT

        pop     {r1-r12, pc}             @ restore the context

@ Define constant values here if necessary
COUNT_ADD: .word COUNT
</code></pre><h2>Appendix 1: Supplementary Graphs</h2>
<p><img src="http://joeltong.org/ee2024-lab-1/Graphs%20Extra%20copy.jpg" style="width:100%;" /></p>
