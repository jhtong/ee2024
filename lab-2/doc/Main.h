
/** Tentative header definition for assignment 2 **/

typedef state {
    STATE_CALIBRATE;
    STATE_STANDBY;
    STATE_READY;
} state_t;

typedef MESSAGE {
    HOT;
    NORMAL;
    RISKY;
    SAFE;
} message_t;

typedef sensorStatus {
    OK          = 0;
    DANGER      = 1;
} sensorStatus_t;


typedef struct sensorAccelerometer {
    float           value;
    unsigned int    status;
} sensorAccelero_t;


typedef struct sensorLight {
    float           value;
    unsigned int    status;

} sensorLight_t;

typedef struct sensorFrequency {
    float           value;
    unsigned int    status;
    float           threshold;      // for noise

} sensorFreq_t;

/** sensor_light **/
* sensorLight_t     sensorLight_new();
void                sensorLight_update(sensorLight_t* sensor, float value);
static void         sensorLight_setStatus(sensorLight_t* sensor, float value);
/** sensor_accelerometer  **/
* sensorAccelero_t  sensorAccelero_new();
void                sensorAccelero_update(sensorAccelero_t* sensor, float value);
static void         sensorAccelero_setStatus(sensorAccelero_t* sensor, float value);

/** sensor_frequency **/
* sensorFreq_t      sensorFreq_new(float threshold);
void                sensorFreq_update(sensorFreq_t* sensor, float value);
static void         sensorFreq_setStatus(sensorFreq_t* sensor, float value);


/** buttons **/
typedef btn {
    _Bool value;
    long lastPressed;       // in ms
} btn_t;

* btn_t             btn_new();
static _Bool        btn_debounce(*btn_t button);
_Bool               btn_isPressed(*btn_t button);


/** watchdog **/
typedef struct timer {
    unsigned long long ticks;
    long interval;
} timer_t;

* timer_t           watchdog_new(long interval);
void                watchdog_attachEvent(void (* event)() );
void                watchdog_detachEvent(void (* event)() );
static void         watchDog_onTick();

/** OLED **/
typedef oled {
    state_t state;
} oled_t;

* oled_t            oled_new();
void                oled_print(*oled_t oled);

/** 7seg **/
typedef sevenSeg {
    value;
} sevenSeg_t;

* sevenSeg_t sevenSeg_new();
void sevenSeg_print(* sevenSeg_t device);
