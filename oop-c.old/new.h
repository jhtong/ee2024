#ifndef _NEW_H
#define _NEW_H 

#include <stddef.h>

/** Defines function helpers to manage class instances **/

void *      new (const void * class, ...);
void        delete (void * self);
void *      clone (const void * self);
int         differ (const void * self, const void * b);
size_t      sizeOf (const void * self);

#endif

