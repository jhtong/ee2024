#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "StateManager.h"
#include "State.h"
#include "new.h"
#include "new.r"

static void * StateManager_ctor (void * _self, va_list * app) 
{
    struct StateManager * self = _self;
    /** Begin alloc internal vars here **/
    /**                                **/
    /** End alloc internal vars here   **/
    return self;
}

static void * StateManager_dtor (void * _self) 
{
    struct StateManager * self = _self;
    /** Begin Free internal vars here **/
    /**                               **/
    /** End Free internal vars here   **/
    unsigned int i;
    for (i = 0; i < self->numStates; i++) {
        delete (self->states[i]);
    }
    return self;
}

static void * StateManager_clone (const void * _self) 
{
    // is like a singleton, should not be cloned
    return NULL;
}

static int StateManager_differ(const void * _self, const void * _b)
{
    // is like a singleton, should not be differed
    return 0;
}

static const struct Class _StateManager = {
    sizeof(struct State),
    StateManager_ctor, 
    StateManager_dtor,
    StateManager_clone,
    StateManager_differ
};

/** Expose class **/
const void * StateManager = &_StateManager;


/***********************************************/
/****   Public custom functions go here     ****/
/***********************************************/

_Bool StateManager_stateExists (const void * self, State_t * state)
{
    unsigned int i = 0;
    const StateManager_t * _self = self;

    assert (state);

    char * a, * b;
    for (i = 0; i < _self->numStates; i++) {
        a = State_getName( _self->states[i] );
        b = State_getName( state );
        if (strcmp (a,b) == 0 )
            return 1;
    }
    return 0;
}


_Bool StateManager_stateExistsByName (const void * self, const char * stateName)
{
    unsigned int i = 0;
    const StateManager_t * _self = self;

    assert (stateName);

    char * currName;
    for (i = 0; i < _self->numStates; i++) {
        currName = State_getName( _self->states[i] );
        if (strcmp (stateName, currName) == 0 )
            return 1;
    }
    return 0;
}


State_t * StateManager_getStateByName (const void * self, const char * stateName)
{
    unsigned int i = 0;
    const StateManager_t * _self = self;

    assert (stateName);

    char * currName;
    for (i = 0; i < _self->numStates; i++) {
        currName = State_getName( _self->states[i] );
        if (strcmp (stateName, currName) == 0 )
            return _self->states[i];
    }
    return NULL;
}

/** States are not removable once added 
 * to State Manager (until destruction) 
**/
_Bool StateManager_addState( void * self, State_t * state ) 
{
    StateManager_t * _self = self;
    assert(state);

    if ( StateManager_stateExists(self, state) )
        return 0;

    if ( _self->numStates == MAX_STATES )
        return 0;

    _self->states [ _self->numStates ] = state;
    _self->numStates++;
    return 1;
}


void StateManager_goto (void * self, const char * state_name) 
{
    StateManager_t * _self = self;
    assert (state_name);
    assert (StateManager_stateExistsByName(_self, state_name) == 1);

    _self->stateNext = (char *) state_name;
}


/** Default state is the first state loaded **/
void StateManager_begin (void * self) 
{
    StateManager_t * _self = self;
    _self->stateNow = _self->stateNext = State_getName( _self->states[0] );
}


void StateManager_tick (void * self) 
{
    StateManager_t * _self = self;
    char * now = _self->stateNow;
    char * next = _self->stateNext;
    State_t * nowState = StateManager_getStateByName( _self, now );
    State_t * nextState = StateManager_getStateByName( _self, next );

    if ( strcmp(now, next) != 0 ) {
        nowState->on_exit();
        nextState->on_enter();
    }

    _self->stateNow = _self->stateNext;
    // needed as may have changed
    nowState = StateManager_getStateByName( _self, _self->stateNow );
    nowState->on_tick();
}
