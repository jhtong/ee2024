#include <stdio.h>
#include "State.h"
#include "StateManager.h"
#include "new.h"

void tickHandler() { puts("state 0: entered tick"); }
void enterHandler() { puts("state 0: entered Enter"); }
void endHandler() { puts("state 0: entered End"); }

void tickHandler1() { puts("state 1: entered tick"); }
void enterHandler1() { puts("state 1: entered Enter"); }
void endHandler1() { puts("state 1: entered End"); }

int main() 
{
    // The state manager...

    // Should be initializable
    State_t * a = new(State, "state0", tickHandler, enterHandler, endHandler);
    State_t * b = new(State, "state1", tickHandler1, enterHandler1, endHandler1);
    StateManager_t * sm = new (StateManager);

    // Should be able to check the existence of a state
    printf("State exists? %d\n", StateManager_stateExists(sm, a) );

    // Should be able to check the existence of a state by string name
    printf("State exists? %d\n", StateManager_stateExistsByName(sm, "state0") );

    // Should be able to add states
    printf("%d\n", StateManager_stateExists(sm, a) );
    StateManager_addState (sm, a);
    printf("%d\n", StateManager_stateExists(sm, a) );

    // Should not add duplicate states
    printf("Number of states: %d\n", sm->numStates);
    StateManager_addState (sm, a);
    printf("Number of states: %d\n", sm->numStates);

    // Should be able to add distinct states
    printf("Number of states: %d\n", sm->numStates);
    StateManager_addState (sm, b);
    printf("Number of states: %d\n", sm->numStates);

    // Should be able to run states
    printf("Number of states: %d\n", sm->numStates);
    StateManager_begin (sm);
    StateManager_tick (sm);
    StateManager_tick (sm);
    StateManager_tick (sm);

    // Should be able to navigate between states
    StateManager_tick (sm);
    StateManager_goto (sm, "state1");
    StateManager_tick (sm);
    StateManager_tick (sm);
    StateManager_tick (sm);
    StateManager_goto (sm, "state0");
    StateManager_tick (sm);
    StateManager_tick (sm);

    // Should be freeable
    delete(sm);

    //printf("str: %s\n", State_getName(a));
    //State_attachHandler(a, ON_TICK, endHandler);
    //printf("str: %s\n", a->name);
    //State_runHandler(a, ON_TICK);
    //a->on_tick();

    //someFn(START);
    //const void * State bla = (const void * State) a;
    //delete(a);
    return 0;
}
