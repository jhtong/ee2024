#ifndef _STATE_H
#define _STATE_H

#include <stdbool.h>

typedef struct State {
    const void * class;
    /** Accessible variables go here **/
    char * name;
    void (* on_tick)();
    void (* on_enter)();
    void (* on_exit)();
} State_t;


typedef enum Event_State {
    ON_TICK ,
    ON_ENTER ,
    ON_EXIT
} Event_State_t;


extern const void           * State;


/******************************************/  
/**     Publicly-accessible functions    **/
/******************************************/  

char *                      State_getName (const void * self);
_Bool                       State_attachHandler ( void * self, Event_State_t event, void (* handler)() );
void                        State_runHandler (void * self, Event_State_t event);

#endif
