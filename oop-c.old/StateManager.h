#ifndef _STATE_MANAGER_H
#define _STATE_MANAGER_H

#define MAX_STATES              10

#include <stdbool.h>

#include "State.h"

typedef struct StateManager {
    const void * class;
    /** Accessible variables go here **/
    State_t *           states[MAX_STATES];
    char *              stateNow;
    char *              stateNext;
    unsigned int        numStates;

} StateManager_t;

extern const void       * StateManager;

/******************************************/  
/**     Publicly-accessible functions    **/
/******************************************/  

/**   For setup / retrieving states   **/
_Bool                   StateManager_stateExists (const void * self, State_t * state);
_Bool                   StateManager_stateExistsByName (const void * self, const char * state_name);
State_t *               StateManager_getStateByName (const void * self, const char * state_name);
_Bool                   StateManager_addState ( void * self, State_t * state );

/**   For navigating between states   **/ 
void                    StateManager_begin (void * self);  // initialization
void                    StateManager_tick (void * self);
void                    StateManager_goto (void * self, const char * state_name);

#endif
