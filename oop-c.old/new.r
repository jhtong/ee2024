#ifndef _NEW_R
#define _NEW_R

/** Definition of Class representation **/

#include <stdarg.h>
#include <stdio.h>

struct Class {
    size_t      size;
    void *      (* ctor) (void * self, va_list * app);
    void *      (* dtor) (void * self);
    void *      (* clone) (const void * self);
    int         (* differ) (const void * self, const void * b);
};
#endif
