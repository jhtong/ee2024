/*
 * Vector_Data.c
 *
 *  Created on: Mar 11, 2014
 *      Author: joel
 */

#ifndef _VECTOR_DATA_C_
#define _VECTOR_DATA_C_

#include <stdint.h>
#include <stdbool.h>

typedef struct Vector_Data {
	/** Buttons **/
	_Bool btn_calibrate;

	/** ms ticks **/
	volatile uint32_t msTicks;
	volatile uint32_t previousMs;
	volatile uint32_t delayMs;

	/** Sensor input **/
	uint32_t light;
	float temperature;

	int8_t zAccel;
	int32_t zoff;
	int32_t vibrationFreq;

	/** Conditionals **/
	volatile _Bool isHot;
	volatile _Bool isRisky;
	volatile int32_t standbyCounter;
	volatile _Bool warningOn;


} Vector_Data_t;

volatile Vector_Data_t FFSData;

#endif /* VECTOR_DATA_C_ */
