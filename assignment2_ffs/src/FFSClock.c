#include <stdlib.h>
#include <stdio.h>

#include "lpc17xx_timer.h"

#include "FFSClock.h"
#include "Vector_Data.h"


void SysTick_Handler(void) {
	FFSData.msTicks++;
}

void init_sysTick() {
	if (SysTick_Config(SystemCoreClock / 1000)) {
		/** failed to initialize interrupt handler **/
		while (1);
	}
}

void SysTick_delay(uint32_t delayTicks) {
	uint32_t currentTicks;
	currentTicks = FFSData.msTicks;
	while ((FFSData.msTicks - currentTicks) < delayTicks);
}

uint32_t SysTick_getMsElapsed() {
	return FFSData.msTicks;
}


