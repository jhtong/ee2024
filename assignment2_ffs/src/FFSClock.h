#ifndef _FFS_CLOCK
#define _FFS_CLOCK

#include <stdint.h>

void SysTick_Handler(void);
void SysTick_delay(uint32_t delayTicks);
uint32_t SysTick_getMsElapsed(); 				// wrapper

// to call!
void init_sysTick();


#endif
