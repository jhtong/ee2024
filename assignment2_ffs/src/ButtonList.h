#ifndef _BUTTON_LIST_H
#define _BUTTON_LIST_H

#define MAX_BUTTONS 					10

#include <stdbool.h>

/**
 * ButtonList singleton class manages an array of buttons.  It is similar to a vector table, and has descriptions
 * on port / pin.  It is also an event manager.
 *
 * Each button requires an event handler passed to it.  This handler is run when the specified GPIO port / pin
 * registers a high (e.g. pushbutton is pressed).
 *
 * See main.c for an example.
 */

typedef struct Button {
	uint32_t port;
	uint32_t pin;
	void (*handler)(void);
} Button_t;

typedef struct ButtonList {
	Button_t * volatile buttons[MAX_BUTTONS];
	uint32_t numBtns;
} ButtonList_t;

/******************************************/
/**     Publicly-accessible functions    **/
/******************************************/

void ButtonList_delete();
void ButtonList_attachButton(uint32_t port, uint32_t pin, void (*handler)());
_Bool ButtonList_getButtonState(uint32_t port, uint32_t pin);

// needs to be defined in another file calling it
extern void EINT0_IRQHandler(void);

#endif
