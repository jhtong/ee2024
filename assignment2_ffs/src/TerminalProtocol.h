/*
 * TerminalProtocol.h
 *
 *  Created on: Mar 29, 2014
 *      Author: joel
 */

#ifndef TERMINALPROTOCOL_H_
#define TERMINALPROTOCOL_H_


// access by using TermProtocol[TermProtocolNo.TYPE]

typedef enum TermProtocolNo {
	RDY,
	RNACK,
	RACK,
	HSHK,
	RSTC,
	CACK,
	RSTS,
	SACK,
	REPT
} TerminalProtocol_t;

const char * TermProtocol[] = {
	"RDY",
	"RNACK",
	"RACK",
	"HSHK",
	"RSTC",
	"CACK",
	"RSTS",
	"SACK",
	"REPT"
};

#endif /* TERMINALPROTOCOL_H_ */
