#include "LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"
#include "lpc17xx_i2c.h"
#include "lpc17xx_ssp.h"
#include "lpc17xx_timer.h"

#include "joystick.h"
#include "pca9532.h"
#include "acc.h"
#include "oled.h"
#include "rgb.h"
#include "led7seg.h"
#include "temp.h"
#include "light.h"
#include "Vector_Data.h"

#include <stdio.h>
#include <stdlib.h>


// For 7SEG timer:
//volatile uint32_t msTicks;
volatile uint32_t previousMs;
volatile uint32_t delayMs;

/* Calibration mode: */

 void calibration_init(void);
 void calibration_saveOffset(volatile Vector_Data_t* data);
 void calibration_displayAccel(uint8_t zAcc);

/* Standby Mode: */

 void standby_init(void);
 void standby_countDown(volatile Vector_Data_t* data);
 void displayTempLux(volatile Vector_Data_t* data);

/* Active Mode */
 void rgbRed_init();
 void rgbRed_on();
 void rgbRed_off();
 void active_init(void);
 void readStoreAccel(volatile Vector_Data_t* data);

/** For vibration calculation **/
int vibrations_cmpFn(const void * a, const void * b);
int8_t vibrations_getSmoothedAverage(int8_t rawInput);
void vibrations_updateFrequency(volatile Vector_Data_t * data);

/** For retrieving data from sensor. Intervals needed so we do not hang program. **/
void temperature_update(volatile Vector_Data_t * data);
void light_update(volatile Vector_Data_t * data);

/* Other stuff: */
 void init_ssp(void);
 void init_i2c(void);

 /** For warning **/
void warning_on();
void warning_off();
void leds_onWarning();
void leds_offWarning();
