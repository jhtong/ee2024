/*
 * SensorLight.c
 *
 *  Created on: Mar 25, 2014
 *      Author: joel
 */

#include "SensorLight.h"

#include <stdint.h>

#include "LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "light.h"
#include "FFS_Config.h"
#include "Vector_Data.h"


void EINT3_IRQHandler() {
	if (LPC_GPIOINT->IO2IntStatF >> 5 & 1) {
		LPC_GPIOINT->IO2IntClr |= (1 << 5);
        light_clearIrqStatus();
	}

	// goto state
	FFSData.isHot = 1;
}

void SensorLight_config() {
	light_enable();
	light_setRange(LIGHT_RANGE_1000);

	// configure PINSEL
	PINSEL_CFG_Type PinCfg;
	PinCfg.Funcnum = 0;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = 2;
	PinCfg.Pinnum = 5;
	PINSEL_ConfigPin(&PinCfg);

	// config GPIO 2.5 for temp pin
	GPIO_SetDir(2, 1 << 5, 0);

	SensorLight_setThreshold(LIGHT_LOWER_BOUND, LIGHT_UPPER_BOUND);
	SensorLight_setInterrupts();
}

void SensorLight_setThreshold(uint32_t min, uint32_t max) {
	light_setLoThreshold(min);
	light_setHiThreshold(max);
}

void SensorLight_setInterrupts() {
	light_clearIrqStatus();
	LPC_GPIOINT->IO2IntEnF |= (1 << 5);
	NVIC_EnableIRQ(EINT3_IRQn);
}

void SensorLight_delete() {
	light_shutdown();
}
