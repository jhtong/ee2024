/*
 * SensorLight.h
 *
 *  Created on: Mar 25, 2014
 *      Author: joel
 */

#ifndef SENSORLIGHT_H_
#define SENSORLIGHT_H_

#include <stdint.h>

#include "light.h"
#include "StateManager.h"

extern StateManager_t * sm;

void SensorLight_config();
void SensorLight_setThreshold(uint32_t min, uint32_t max);
void SensorLight_setInterrupts();
void SensorLight_delete();

#endif /* SENSORLIGHT_H_ */
