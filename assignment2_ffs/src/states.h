#ifndef _STATES_H
#define _STATES_H

#include <stdlib.h>
#include <stdbool.h>

#include "State.h"


/** Public functions and necessary variables **/

State_t * state_0, * state_1, * state_2;

void tickHandler();
void enterHandler();
void endHandler();

void tickHandler1();
void enterHandler1();
void endHandler1();

void init_states();
void free_states();


#endif
