/*
 * Terminal.h
 *
 *  Created on: Mar 29, 2014
 *      Author: joel
 */

#ifndef TERMINAL_H_
#define TERMINAL_H_

#include <stdbool.h>
#include <stdint.h>

#include "FFS_Config.h"

typedef enum {
    RECIPIENT_FFS,
    RECIPIENT_BASE
} Author_t;

typedef struct {
    Author_t to;
    Author_t from;
    uint8_t length;
    char ** tokens;
} Message_t;


typedef struct {
    char * buffer;
    uint32_t buffLen;
    Message_t * in;
    Message_t * out;
    uint32_t timeLastMsg;
    uint32_t timeElapsed;
} Terminal_t;


///////////////////////////////////////////////////////////////////////////////////////

/** Public functions **/
Message_t * Message_new(Author_t to, Author_t from, const char * payload);
void Message_delete(Message_t * self);
char * Message_get(Message_t * self, _Bool addNewline);

Terminal_t * Terminal_new();
void Terminal_delete(Terminal_t * self);
void Terminal_harvest(Terminal_t * self, uint8_t * buffer, uint32_t length);
_Bool Terminal_hasMessage(Terminal_t * self);
void Terminal_send(Terminal_t * self, const char * payload);
uint32_t Terminal_getTimeElapsed(Terminal_t * self);
Message_t * Terminal_retrieve(Terminal_t * self);
void Terminal_resetTime(Terminal_t * self);

#endif /* TERMINAL_H_ */
