#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "LPC17xx.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_gpio.h"

#include "ButtonList.h"
#include "FFSClock.h"


/***********************************************/
/****   Public custom functions go here     ****/
/***********************************************/

static volatile ButtonList_t * _btnList = NULL;

ButtonList_t * ButtonList_new();

ButtonList_t * ButtonList_getInstance() {
	if (!_btnList)
		_btnList = ButtonList_new();
	return _btnList;
}


ButtonList_t * ButtonList_new() {
	ButtonList_t * self = calloc(1, sizeof(ButtonList_t));
	return self;
}

void ButtonList_delete() {
	ButtonList_t * self = ButtonList_getInstance();
	uint32_t i;
	for (i = 0; i < self->numBtns; i++) {
		free(self->buttons[i]);
	}
	free(self);
}

static _Bool ButtonList_exists(uint32_t port, uint32_t pin) {
	ButtonList_t * self = ButtonList_getInstance();
	uint32_t i;

	for (i = 0; i < self->numBtns; i++) {
		Button_t * curBtn = (Button_t *) self->buttons[i];
		if (curBtn->pin == pin && curBtn->port == port)
			return 1;
	}
	return 0;
}

void ButtonList_attachButton(uint32_t port, uint32_t pin, void (*handler)()) {
	ButtonList_t * self = ButtonList_getInstance();
	if (ButtonList_exists(port, pin)) {
		return;
	}

	Button_t * btn = calloc(1, sizeof(Button_t));
	self->buttons[self->numBtns] = btn;
	self->numBtns++;

	btn->pin = pin;
	btn->port = port;
	btn->handler = handler;

	PINSEL_CFG_Type PinCfg;
//	PinCfg.Funcnum = 0;
	PinCfg.Funcnum = 1;
	PinCfg.OpenDrain = 0;
	PinCfg.Pinmode = 0;
	PinCfg.Portnum = port;
	PinCfg.Pinnum = pin;
	PINSEL_ConfigPin(&PinCfg);

	GPIO_SetDir(port, 1 << pin, 0);

//	LPC_GPIOINT->IO2IntEnR |= (1<<10);
//	LPC_GPIOINT->IO2IntEnF ^= ~(1<<10);

	NVIC_EnableIRQ(EINT0_IRQn);

}

_Bool ButtonList_getButtonState(uint32_t port, uint32_t pin) {
	return ! ((GPIO_ReadValue(port) >> pin) & 0x01);
}

