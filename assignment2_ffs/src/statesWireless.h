#ifndef STATES_WIRELESS_H
#define STATES_WIRELESS_H

#include "State.h"
#include "Terminal.h"

void wState_authenticateTick();
void wState_authenticateBegin();
void wState_authenticateEnd();

/** States **/
State_t * wState_auth;
State_t * wState_active;

/** Public functions **/
void init_wirelessStates();
void free_wirelessStates();

#endif
