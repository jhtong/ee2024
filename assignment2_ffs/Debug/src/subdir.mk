################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ButtonList.c \
../src/FFSClock.c \
../src/SensorLight.c \
../src/State.c \
../src/StateManager.c \
../src/Terminal.c \
../src/cr_startup_lpc17.c \
../src/functions.c \
../src/main.c \
../src/states.c \
../src/statesWireless.c 

OBJS += \
./src/ButtonList.o \
./src/FFSClock.o \
./src/SensorLight.o \
./src/State.o \
./src/StateManager.o \
./src/Terminal.o \
./src/cr_startup_lpc17.o \
./src/functions.o \
./src/main.o \
./src/states.o \
./src/statesWireless.o 

C_DEPS += \
./src/ButtonList.d \
./src/FFSClock.d \
./src/SensorLight.d \
./src/State.d \
./src/StateManager.d \
./src/Terminal.d \
./src/cr_startup_lpc17.d \
./src/functions.d \
./src/main.d \
./src/states.d \
./src/statesWireless.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__USE_CMSIS=CMSISv1p30_LPC17xx -D__CODE_RED -D__REDLIB__ -I"/home/joel/repos/EE2024/Lib_CMSISv1p30_LPC17xx/inc" -I"/home/joel/repos/EE2024/Lib_EaBaseBoard/inc" -I"/home/joel/repos/EE2024/Lib_MCU/inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


