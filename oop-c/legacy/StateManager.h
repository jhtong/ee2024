#ifndef _STATE_MANAGER_H
#define _STATE_MANAGER_H

#define MAX_STATES              10

#include <stdbool.h>
#include <stdint.h>

#include "State.h"

typedef struct StateManager {
    const void * class;
    /** Accessible variables go here **/
    uint32_t              stateNow;
    uint32_t              stateNext;
    uint32_t        	  numStates;
    State_t *             states[MAX_STATES];

} StateManager_t;

extern const void       * StateManager;

/******************************************/  
/**     Publicly-accessible functions    **/
/******************************************/  

/**   For setup / retrieving states   **/
_Bool                   StateManager_stateExists (State_t * state);
State_t *                StateManager_getState (const uint32_t stateNumber);
_Bool                   StateManager_addState (State_t * state );

/**   For navigating between states   **/ 
void                    StateManager_begin ();  // initialization
void                    StateManager_tick ();
void                    StateManager_goto (uint32_t stateNumber);

// retrieve instance of "singleton"
StateManager_t * 		StateManager_getInstance();

#endif
