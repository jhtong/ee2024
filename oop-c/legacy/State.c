#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "State.h"
#include "new.h"
#include "new.r"

static void * State_ctor (void * _self, va_list * app)
{
    struct State * self = _self;
    /** Begin alloc internal vars here **/
    /**                                **/
    /** End alloc internal vars here   **/

    self->on_tick = va_arg(*app, void *);
    self->on_enter = va_arg(*app, void *);
    self->on_exit = va_arg(*app, void *);
    return self;
}

static void * State_dtor (void * _self)
{
    struct State * self = _self;
    /** Begin Free internal vars here **/
    /**                               **/
    /** End Free internal vars here   **/
    self->on_tick = self->on_enter = self->on_exit = NULL;
    return self;
}

static void * State_clone (const void * _self)
{
    const struct State * self = _self;
    return new(State, self->on_tick, self->on_enter, self->on_exit);
}

static int State_differ(const void * _self, const void * _b)
{
    const struct State * self = _self;
    const struct State * b = _b;
    if (self == b) {
        return 0;
    }

    if (self->on_enter != b->on_enter)
    	return 1;

    if (self->on_tick != b->on_tick)
    	return 1;

    if (self->on_exit != b->on_exit)
    	return 1;

    return 0;

}

static const struct Class _State = {
    sizeof(struct State),
    State_ctor,
    State_dtor,
    State_clone,
    State_differ
};

/** Expose class **/
const void * State = &_State;


/***********************************************/
/****   Public custom functions go here     ****/
/***********************************************/

_Bool State_attachHandler (void * self, Event_State_t event, void (* handler)() )
{
    assert (handler);
    struct State * _self = self;

    if (event == ON_TICK) {
        _self->on_tick = handler;

    } else if (event == ON_ENTER) {
        _self->on_enter = handler;

    } else if (event == ON_EXIT) {
        _self->on_exit = handler;
    } else
        return false;

    return true;
}


void State_runHandler (void * self, Event_State_t event)
{
    struct State * _self = self;
    assert (_self->on_tick && _self->on_enter && _self->on_exit);

    if (event == ON_TICK) {
        _self->on_tick();

    } else if (event == ON_ENTER) {
        _self->on_enter();

    } else if (event == ON_EXIT) {
        _self->on_exit();
    }
}
