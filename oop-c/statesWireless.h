#ifndef STATES_WIRELESS_H
#define STATES_WIRELESS_H

#include "State.h"
#include "Terminal.h"

void wStates_gather();

void wState_authenticateTick();
void wState_authenticateBegin();
void wState_authenticateEnd();

Terminal_t * tm;
State_t * wState_auth;

void init_wirelessStates();
void free_wirelessStates();

#endif
