#include <assert.h>
#include <stdlib.h>
#include <stdint.h>

#include "State.h"
#include "StateManager.h"


StateManager_t * StateManager_new() {
    StateManager_t * sm = calloc(1, sizeof(StateManager_t));
    sm->refNow = sm->refNext = sm->numStates = 0;
}

void StateManager_delete( StateManager_t * sm) {
    uint32_t i = sm->refNow;
    sm->states[i]->on_exit();
    free(sm);
}

void StateManager_addState(StateManager_t * sm, State_t * state) {
//    assert ( (sm->numStates >= 0) && (sm->numStates < MAX_STATES) );
    sm->states[sm->numStates] = state;
    sm->numStates++;
}


/** needed to kick-start the first state.  Then run StateManager_tick() **/
void StateManager_kick(StateManager_t * sm) {
    sm->states[0]->on_enter();
}


void StateManager_tick(StateManager_t * sm) {
    if (sm->refNow != sm->refNext) {
        sm->states[sm->refNow]->on_exit();
        sm->refNow = sm->refNext;
        sm->states[sm->refNow]->on_enter();
    } 

    sm->states[sm->refNow]->on_tick();
}

void StateManager_goto(StateManager_t * sm, uint32_t ref) {
    sm->refNext = ref;
}
