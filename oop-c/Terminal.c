/*
 * Terminal.c
 *
 *  Created on: Mar 29, 2014
 *      Author: joel
 */

#include "Terminal.h"

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <stdio.h>

#define MAX_CHUNKS					10

Message_t * Message_new(Author_t to, Author_t from, const char * payload) {

    // clone string
    char * _payload;
    _payload = calloc(1, strlen(payload) + 1);
    strcpy(_payload, payload);

    // tokenize string
    uint8_t length = 0;
    char ** tmpMsg;
    char * chunk = NULL;
    tmpMsg = calloc(MAX_CHUNKS, sizeof(char *));

    chunk = strtok(_payload, " ");
    tmpMsg[0] = strdup(chunk);
    if (chunk != NULL) length++;

    while(chunk = strtok(NULL, " ")) {
        tmpMsg[length] = strdup(chunk);
        length++;
    }

    // free unused memory
    uint8_t i;
    for( i = length; i < (uint8_t) MAX_CHUNKS; i++)
        free(tmpMsg[i]);
    free(_payload);
    free(chunk);

    // create message
    Message_t * self = calloc(1, sizeof(Message_t));
    self->to = to;
    self->from = from;
    self->length = length;
    self->tokens = tmpMsg;

    return self;
}

void Message_delete(Message_t * self) {
    uint8_t i = 0;
    for(i = 0; i < self->length; i++) {
        free(self->tokens[i]);
    }
    free(self);
}

char * Message_get(Message_t * self, _Bool addNewline) {
    char * buffer;
    buffer = calloc(1, 256);
    uint8_t i;
    strcat(buffer, self->tokens[i]);
    for (i = 1; i < self->length; i++) {
        strcat(buffer, " ");
        strcat(buffer, self->tokens[i]);
    }
    if (addNewline) strcat(buffer, "\n");
    return buffer;
}

Terminal_t * Terminal_new() {
    Terminal_t * self = calloc(1, sizeof(Terminal_t));
    self->buffer = calloc(1, 256);
    return self;
}

void Terminal_delete(Terminal_t * self) {
    free(self->buffer);
    if (self->in) Message_delete(self->in);
    if (self->out) Message_delete(self->out);
    free(self);
}

/** Collects information and creates message, if input is ready **/
void Terminal_harvest(Terminal_t * self, uint8_t * buffer, uint32_t length) {

    if (length == 0) return;
    char * offset = (char *) self->buffer + self->buffLen;
    memcpy(offset, buffer, length);
    self->buffLen += length;

    // create new message if end sequence detected
    if (self->buffer[self->buffLen-1] == '\n') {
        if(self->in) free(self->in);
        self->in = Message_new(RECIPIENT_FFS, RECIPIENT_BASE, self->buffer);
        self->buffLen = 0;
        // reset buffer 
        free(self->buffer);
        self->buffer = calloc(1, 256);
    }
}

/** Checks if there is any incoming message **/
_Bool Terminal_hasMessage(Terminal_t * self) {
    return self->in != NULL;
}

void Terminal_resetTime(Terminal_t * self) {
    // reset the time here
}

uint32_t Terminal_getTimeElapsed(Terminal_t * self) {
    //TODO: Return elapsed time here
}

/** Sends a char string **/
void Terminal_send(Terminal_t * self, const char * payload) {
    if (self->out) free(self->out);
    Message_t * msg = Message_new(RECIPIENT_BASE, RECIPIENT_FFS, payload);
    const char * _payload = Message_get(msg, true);
    Terminal_resetTime(self);               // reset timeout
    //TODO: send payload via uart
}

/** 
 * Returns message if any, and empties inbox 
 * NOTE: Message pointer must be deleted after use, to minimize occurence of
 * memory leak
 * **/
Message_t * Terminal_retrieve(Terminal_t * self) {
    if (!self->in) return NULL;
    Message_t * result = self->in;
    self->in = NULL;
    return result;
}

