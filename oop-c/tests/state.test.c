/** State class BDD tests **/

#include <stdio.h>
#include "State.h"
#include "new.h"

// some hooks to try 
void tickHandler() {
    puts("entered tick");
}

void enterHandler() {
    puts("entered Enter");
}

void endHandler() {
    puts("entered End");
}


int main() 
{
    // A state..

    // should be instantiable
    State_t * a = new(State, "someFn", tickHandler, enterHandler, endHandler);

    // should be able to have handlers attached
    State_attachHandler(a, ON_TICK, endHandler);

    // should be able to execute handlers
    State_runHandler(a, ON_TICK);

    // and execute handlers direct from struct (NOT AS SAFE)
    a->on_tick();

    // should have memory leaks.
    delete(a);

    return 0;
}
