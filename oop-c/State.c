#include <assert.h>
#include <stdbool.h>
#include <stdlib.h>

#include "State.h"

State_t * State_new( void (* on_tick)(), void (*on_enter)(), void (* on_exit)() ) {
    State_t * state = (State_t *) calloc( 1, sizeof(State_t) );
    state->on_tick = on_tick;
    state->on_enter = on_enter;
    state->on_exit = on_exit;
    return state;
}


void State_delete(State_t * state) {
    free(state);
}
