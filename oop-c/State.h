#ifndef _STATE_H
#define _STATE_H

#include <stdbool.h>

typedef struct State {
    void (* on_tick)();
    void (* on_enter)();
    void (* on_exit)();
} State_t;


typedef enum Event_State {
    ON_TICK ,
    ON_ENTER ,
    ON_EXIT
} Event_State_t;


/******************************************/  
/**     Publicly-accessible functions    **/
/******************************************/  

State_t * State_new( void (* on_tick)(), void (* on_enter)(), void (* on_exit)()  );
void State_delete(State_t * state);

#endif
