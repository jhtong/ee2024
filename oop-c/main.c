#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "TerminalProtocol.h"
#include "Terminal.h"

#include "statesWireless.h"
#include "StateManager.h"

int main() 
{
    //Terminal_t * tty = Terminal_new();

    //printf("out: %s\n", tty->out);
    /**
    char g[] = "cats are fat";
    Message_t * msg = Message_new(RECIPIENT_FFS, RECIPIENT_BASE, g);
    char * output = Message_get(msg, true);
    Message_delete(msg);

    Terminal_t * tm = Terminal_new();
    uint8_t buff[] = {'h', 'e', 'l'};
    uint8_t buff2[] = {'l', 'o', '\r'};
    uint8_t buff3[] = {'\n'};
    Terminal_harvest(tm, buff, 3);
    Terminal_harvest(tm, buff2, 3);
    Terminal_harvest(tm, buff3, 1);

    Terminal_retrieve(tm);

    Terminal_delete(tm);
    puts("done");
    **/

    init_wirelessStates();
    StateManager_t * wm;
    wm = StateManager_new();
    StateManager_addState(wm, wState_auth);
    StateManager_kick(wm);
    while(1) {
        StateManager_tick(wm);
    }
    free_wirelessStates();

    return 0;
}
