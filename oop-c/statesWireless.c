#include "statesWireless.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

static Message_t * lastMsg;

void collectInfo() {
    uint8_t buffer[] = {'h', 'i'};
    //uint8_t * buffer[];
    uint32_t length = 0;
    Terminal_harvest(tm, buffer, length);
}

void wState_authenticateTick() {
    collectInfo();

    //timeout 
    if(tm->timeElapsed > 5000 && !Terminal_hasMessage(tm)) {
        Terminal_resetTime(tm);
        Terminal_send(tm, "RDY 013 \r\n");
    }

    // return if no message
    if (!Terminal_hasMessage(tm)) return;

    // else continue
    lastMsg = Terminal_retrieve(tm);
    char ** tokens = lastMsg->tokens;
    
    if (strcmp(tokens[0], "RNACK") == 0) {
        Terminal_send(tm, "RDY 013 \r\n");
    }

    else if (strcmp(tokens[0], "RACK") == 0) {
        Terminal_send(tm, "HSHK 013 \r\n");
        //TODO: Go to handshaking state here
    }

}

void wState_authenticateBegin() {
    Terminal_send(tm, "RDY 013 \r\n");
}

void wState_authenticateEnd() {
}

void init_wirelessStates() {
    tm = Terminal_new();
    wState_auth = State_new(
            wState_authenticateTick, 
            wState_authenticateBegin, 
            wState_authenticateEnd);
}

void free_wirelessStates() {
    State_delete(wState_auth);
}

