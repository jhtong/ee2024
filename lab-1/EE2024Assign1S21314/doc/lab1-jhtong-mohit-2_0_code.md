## Code

### The General Idea

Our gradient descent algorithm in Assembly evaluates the following two expressions:

$$ 
\begin{align}
\nabla f (x_k) &= 2ax_k + b \\
x_{k+1} &= x_k - \lambda \nabla f(x_k)
\end{align}
$$ 

The loop guard for the routine is given by:

$$
x_{k+1} = x_k
$$

From this, we can derive our algorithm as follow, in pseudo-code:

	optimize:
    	1. Store the context
 	   2. Store x_k
    	3. Evaluate Expression 1
        4. Evaluate Expression 2
        5. Jump to <optimize> if x_k !== x_k+1.
        
	found:
        1. Write over x_k with x_k+1
        2. Restore the context
        3. Exit

### Details

#### Scaling

As both $$$x_k$$$ and $$$\lambda$$$ are given as floating-point numbers, we scale the expressions to the scale of `100` for expression (1), and `1000` for expression (2) respectively to preserve precision.  This scaling is necessary, as the ARMv7 instruction set does not come with floating-point support out-of-box.  The expressions are hence:

$$ 
\begin{align}
100 ( \nabla f (x_k) ) &= 100( 2ax_k + b ) \\
1000 (x_{k+1}) &= 1000(x_k - \lambda \nabla f(x_k))
\end{align}
$$

To ensure consistency in scaling, input was first converted to an integer-based version by multiplying the float input with `100`.  Output from `optimize()` was then rescaled back and typecasted to a float.  Care was taken to preserve scaling between the two expressions.  The following properties were used to preserve scaling:

##### Distributive

$$
c (A + B) = cA + cB
$$

##### Associative

$$
cd (AB) = c A \cdot dB
$$

### The Assembly

#### Register Map

The outlay of general-purpose registers is given in the map below, and was used to ensure neat code and minimal use of registers.

| r0 | r1 | r2 | r3 | r4 | r5 | r6 |
| - | - | - | - | - | - | - |
| $$$x_i$$$ | $$$a$$$ | $$$b$$$ | $$$c$$$ | $$$\lambda$$$ | misc. | misc. |

<br>

| r7 | r8 | r9 | r10 | r11 | r12 |
| - | - | - | - | - | - | - | - | - | - | - | - | - |
| misc. | misc. | misc. | misc. | $$$i$$$ | count_add | |



#### Defining constants

Constants were defined by using the `.word` and `.equ` directives.  The difference between the directives is that `.equ` defines a constant, whereas in the case of `.word`, space is allocated on the stack for the constant and initialized.

    @ Equates
        .equ    STEP,        2					@lambda at x10
        .equ    SCALE,      10
        .equ    SCALE2,    100
        .equ    LCOUNT,    0
        
    @ Define constant values here if necessary
        COUNT_ADD: .word COUNT

They are then loaded upon entering the routine:

        @ load constants
        ldr     r11, COUNT_ADD           @ global variable COUNT reference
        ldr     r10, =LCOUNT             @ initalize counting register
        ldr     r8, =SCALE               @ Store 10 in R8
        ldr     r9, =SCALE2              @ Store 100 in R9

#### Performing normal subroutine operations

A routine written in Assembly does not preserve the program's context by default.  The state of the registers (context), and the link register (containing the reference address just before the point of entry into the subroutine) needs to be saved prior to modification of the registers.  Failure to do so might result in a runtime error, for example a hard fault.  As such, we preserve the context by pushing it onto the heap, and pop it from the heap just before returning from the routine:

	push 	{r1-r12,lr} 			@ save the context

#### Computing $$$\nabla f(x)$$$

Computation of the derivative is given as follow.  Care was taken to preserve scaling at x100.

		@ 2ax + b at x100
        lsl     r5, r0, #1               @ (xi*100) * 2, Store in R5
        mul     r7, r2, r9               @ b * 100, Store in R7
        mla     r6, r1, r5, r7           @ a * [(xi*100) * 2] + [b * 100], Store in R6

A logical left shift `LSL` operation was used to multiply by 2, while `MLA` enabled two operations (multiplication and subsequently addition) to be performed, reducing the number of instructions written.  While it is noted that to fully accomodate 32-bit multiplication would require a 2 words (64-bit) space, a 32-bit space was assumed as subsequent iterations would require even larger spaces (e.g. 64-bit multiplication in iteration 2, 4 words).  Hence the upper 32-bits were discarded.


#### Computing the second expression

$$$x_{k+1}$$$ was computed in the second section.  As the formula involved $$$\lambda : \lambda = 0.2 \Rightarrow \lambda \leq 1$$$, and $$$\lambda$$$ (given by step) had to be scaled up by 10.  Hence, the total scaling was x1000.  Care was taken to preserve scaling at that value.  

The finalized version of $$$x_{k+1}$$$ was then renormalized to x100 by performing an `sdiv` operation.

        @ x_k+1 at x1000
        ldr     r4, =STEP                @ Store STEP * 10 in R4
        mul     r6, r6, r4               @ {a * [(xi*100) * 2] + [b * 100]} * (STEP*10), Store in R6

        mul     r7, r0, r8               @ (xi*100) * 10, Store in R7
        sub     r5, r7, r6               @ |(xi*100) * 10| – |{a * [(xi*100) * 2] + [b * 100]} * (STEP*10)|, Store in R5

        @ x_k+1 at x100
        sdiv    r6, r5, r8               @ \\\|(xi*100) * 10| – |{a * [(xi*100) * 2] + [b * 100]} * (STEP*10)|\\\ ÷ 10, Store in R6
                                         @ restore x_k+1 scaling to x100
