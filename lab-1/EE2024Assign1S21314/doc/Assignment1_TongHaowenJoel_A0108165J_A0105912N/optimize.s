     .syntax unified
     .cpu cortex-m3
     .thumb
     .align 2
     .global    optimize
     .global COUNT                       @ global variable
     .thumb_func


@ EE2024 Assignment 1
@ Authors:
@ TONG Haowen Joel <me at joeltong dot org>
@ SHRIDHAR Mohit <mohit at nus dot edu dot sg>

@ Equates
    .equ     STEP,      2                @lambda at x10
    .equ     SCALE,     10
    .equ     SCALE2,    100
    .equ     LCOUNT,    0


optimize:
    @ TODO: THIS VERSION DOES NOT CONSIDER EXCEPTIONS

    @ General Purpose Register Map
    @ r0:         [xk*100], return value
    @ r1:         a
    @ r2:         b
    @ r3:         c

    @ r8:         SCALE 10
    @ r9:         SCALE 100


    preamble:
        push     {r1-r12,lr}             @ save the context

        @ load constants
        ldr     r11, COUNT_ADD           @ global variable COUNT reference
        ldr     r10, =LCOUNT             @ initalize counting register
        ldr     r8, =SCALE               @ Store 10 in R8
        ldr     r9, =SCALE2              @ Store 100 in R9

    loop:
        add     r10, r10, #1             @ increment counter

        @ 2ax + b at x100
        lsl     r5, r0, #1               @ (xk*100) * 2, Store in R5
        mul     r7, r2, r9               @ b * 100, Store in R7
        mla     r5, r1, r5, r7           @ a * [(xk*100) * 2] + [b * 100], Store in R5

        @ x_k+1 at x1000
        ldr     r4, =STEP                @ Store STEP * 10 in R4
        mul     r5, r5, r4               @ {a * [(xk*100) * 2] + [b * 100]} * (STEP*10), Store in R6

        mul     r6, r0, r8               @ (xk*100) * 10, Store in R7
        sub     r5, r6, r5               @ |(xk*100) * 10| – |{a * [(xk*100) * 2] + [b * 100]} * (STEP*10)|, Store in R5

        @ x_k+1 at x100
        sdiv    r6, r5, r8               @ \\\|(xk*100) * 10| – |{a * [(xk*100) * 2] + [b * 100]} * (STEP*10)|\\\ ÷ 10, Store in R6
                                         @ restore x_k+1 scaling to x100

    compare:
        mov     r5, r0                   @ moving for temp comparison
        mov     r0, r6                   @ moving so it can loop back or return val, if needed
        cmp     r6, r5                   @ subtract and trigger sReg if zero
        bne     loop                     @ loop if not equal (Z flag checked) with r0 = x_k+1

        str     r10, [r11]               @ write global varible COUNT

        pop     {r1-r12, pc}             @ restore the context
        bx      lr                       @ return from function call with result in r0

@ Define constant values here if necessary
COUNT_ADD: .word COUNT
