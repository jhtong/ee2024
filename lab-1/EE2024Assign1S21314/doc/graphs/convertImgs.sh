#!/bin/bash 

mkdir -p output

FILES=*.pdf
OUTPUT=output/

for file in $FILES 
do
    echo "Processing $file"
    convert -density 300 -trim -quality 100 $file ${OUTPUT}${file}.jpeg
done
