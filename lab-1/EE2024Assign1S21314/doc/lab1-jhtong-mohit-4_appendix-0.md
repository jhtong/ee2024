## Appendix 0: The Code

**main.c**
	
    /** main.c **/
    
	#include "stdio.h"
    #include "math.h"
    
    // EE2024 Assignment 1
    
    /**
        @ Authors:
        @ TONG Haowen Joel <me at joeltong dot org>
        @ SHRIDHAR Mohit <mohit at nus dot edu dot sg>
    */
    
    // Optimization routine written in assembly language
    // Input parameters (signed integers):
    //     xi : integer version of x
    //     a, b, c : integer coefficients of f(x)
    // Output : returns solution x (signed integer)
    
    unsigned int COUNT; // global variable passed to ASM
    extern int optimize(int xi, int a, int b, int c);
    
    
    int main(void)
    {
        int a=1, b=-14, c=45, xsoli; // Default a, b, c
        float fp, x, xprev, xsol, change, lambda=0.2, perfect;
    
        printf("Enter the parameters of the quadratic function (a[x^2] + b[x] + c) to be optimized\n");
    
        printf("Int a: ");
        scanf("%d", &a);
    
        printf("Int b: ");
        scanf("%d", &b);
    
        printf("Int c: ");
        scanf("%d", &c);
    
        printf("Initial value of x (press [Enter] after keying in): "); // eg: try x0 = -12.35
        scanf("%f", &x);
    
    //  ARM ASM & Integer version
    
        printf("ARM ASM & Integer version:\n");
        xsoli = (int) optimize((int) (x * 100),a,b,c);
        xsol = ( (float) xsoli ) / (100.0);
        printf("Iterative ASM solution : %f (%d iterations)\n",xsol, COUNT);
    
        perfect = (b*-1.0)/(2.0*a);
        printf("Theoretical solution: %f\n\n", perfect);
    
    //  C & Floating Point version
    
        printf("C & Floating point version:\n",x);
        int countC = 0;
        while (1)
        {
            countC++;
    
            fp = 2*a*x + b;
    
            xprev = x;
            change = -lambda*fp;
            x += change;
    
            printf("x: %f, fp: %f, change: %f\n", x, fp, change);
            if (x==xprev) {
                printf("Floating point C Iteration: %f\n", x);
                break;
            }
        }
    
        printf("\nResult: ASM: %f (%d iterations), C floating: %f (%d iterations), Theoretical: %f\n", xsol, COUNT, x, countC, perfect);
    
        // Calculate error:
        float errorAsm = (fabs(perfect-xsol) / (perfect * 1.0)) * 100.0;
        float errorC = (fabs(perfect-x) / (perfect * 1.0)) * 100.0;
        printf("ASM Error: %f%%, C-floating Error: %f%%\n", errorAsm, errorC);
    
        // Enter an infinite loop, just incrementing a counter
        // This is for convenience to allow registers, variables and memory locations to be inspected at the end
        volatile static int loop = 0;
        while (1) {
            loop++;
        }
    }

**optimize.s**

         .syntax unified
         .cpu cortex-m3
         .thumb
         .align 2
         .global    optimize
         .global COUNT                       @ global variable
         .thumb_func
    
    
    @ EE2024 Assignment 1
    @ Authors:
    @ TONG Haowen Joel <me at joeltong dot org>
    @ SHRIDHAR Mohit <mohit at nus dot edu dot sg>
    
    @ Equates
        .equ     STEP,      2                @lambda at x10
        .equ     SCALE,     10
        .equ     SCALE2,    100
        .equ     LCOUNT,    0
    
    
    optimize:
        @ TODO: THIS VERSION DOES NOT CONSIDER EXCEPTIONS
    
        @ General Purpose Register Map
        @ r0:         [xk*100], return value
        @ r1:         a
        @ r2:         b
        @ r3:         c
    
        @ r8:         SCALE 10
        @ r9:         SCALE 100
    
    
        preamble:
            push     {r1-r12,lr}             @ save the context
    
            @ load constants
            ldr     r11, COUNT_ADD           @ global variable COUNT reference
            ldr     r10, =LCOUNT             @ initalize counting register
            ldr     r8, =SCALE               @ Store 10 in R8
            ldr     r9, =SCALE2              @ Store 100 in R9
    
        loop:
            add     r10, r10, #1             @ increment counter
    
            @ 2ax + b at x100
            lsl     r5, r0, #1               @ (xk*100) * 2, Store in R5
            mul     r7, r2, r9               @ b * 100, Store in R7
            mla     r5, r1, r5, r7           @ a * [(xk*100) * 2] + [b * 100], Store in R5
    
            @ x_k+1 at x1000
            ldr     r4, =STEP                @ Store STEP * 10 in R4
            mul     r5, r5, r4               @ {a * [(xk*100) * 2] + [b * 100]} * (STEP*10), Store in R6
    
            mul     r6, r0, r8               @ (xk*100) * 10, Store in R7
            sub     r5, r6, r5               @ |(xk*100) * 10| – |{a * [(xk*100) * 2] + [b * 100]} * (STEP*10)|, Store in R5
    
            @ x_k+1 at x100
            sdiv    r6, r5, r8               @ \\\|(xk*100) * 10| – |{a * [(xk*100) * 2] + [b * 100]} * (STEP*10)|\\\ ÷ 10, Store in R6
                                             @ restore x_k+1 scaling to x100
    
        compare:
            mov     r5, r0                   @ moving for temp comparison
            mov     r0, r6                   @ moving so it can loop back or return val, if needed
            cmp     r6, r5                   @ subtract and trigger sReg if zero
            bne     loop                     @ loop if not equal (Z flag checked) with r0 = x_k+1
    
            str     r10, [r11]               @ write global varible COUNT
    
            pop     {r1-r12, pc}             @ restore the context
    
    @ Define constant values here if necessary
    COUNT_ADD: .word COUNT