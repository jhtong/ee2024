
# EE2024 Assignment 1

| Name | Matric Number |
| -- | -- |
| Joel Haowen **Tong** | A0108165J |
| Mohit **Shridhar** | A0105912N |

## Objective

This report explores the Gradient Descent algorithm to find the minimum for the polynomial $$$ax^2 + bx + c$$$, using both ARM assembly and C.

### Background

The Gradient Descent algorithm finds the minimum of a given function.  It has a complexity of O(n).

If a given function $$$F(x)$$$ is well-defined and differentiable within a neighborhood that encloses the minimum point $$$x_{min}$$$ and $$$x_0$$$, then one should travel in the direction opposite the gradeint to reach the minimum point $$$x_{min}$$$, specifically:

$$
F(x_{k+1}) = F(x_k) - \lambda \nabla F(x) : k \in \mathbb{N}
$$

For sufficiently small values of $$$\lambda < 1$$$, 

$$
F(x_{k+1}) \leq F(x)
$$

The local minimum is given by the terminating condition:

$$
F(x_{n+1}) \approx F(x_n) : \exists \, n : x_{n+1} = x_{min} \bullet n  \in \mathbb{N}
$$

An example of the algorithm in operation is given by:

<img src="http://upload.wikimedia.org/wikipedia/commons/6/60/Banana-SteepDesc.gif" style="width: 300px;"  />

## Code

### The General Idea

Our gradient descent algorithm in Assembly evaluates the following two expressions:

$$ 
\begin{align}
\nabla f (x_k) &= 2ax_k + b \\
x_{k+1} &= x_k - \lambda \nabla f(x_k)
\end{align}
$$ 

The loop guard for the routine is given by:

$$
x_{k+1} = x_k
$$

From this, we can derive our algorithm as follow, in pseudo-code:

	optimize:
    	1. Store the context
 	   2. Store x_k
    	3. Evaluate Expression 1
        4. Evaluate Expression 2
        5. Jump to <optimize> if x_k !== x_k+1.
        
	found:
        1. Write over x_k with x_k+1
        2. Restore the context
        3. Exit

### Details

#### Scaling

As both $$$x_k$$$ and $$$\lambda$$$ are given as floating-point numbers, we scale the expressions to the scale of `100` for expression (1), and `1000` for expression (2) respectively to preserve precision.  This scaling is necessary, as the ARMv7 instruction set does not come with floating-point support out-of-box.  The expressions are hence:

$$ 
\begin{align}
100 ( \nabla f (x_k) ) &= 100( 2ax_k + b ) \\
1000 (x_{k+1}) &= 1000(x_k - \lambda \nabla f(x_k))
\end{align}
$$

To ensure consistency in scaling, input was first converted to an integer-based version by multiplying the float input with `100`.  Output from `optimize()` was then rescaled back and typecasted to a float.  Care was taken to preserve scaling between the two expressions.  The following properties were used to preserve scaling:

##### Distributive

$$
c (A + B) = cA + cB
$$

##### Associative

$$
cd (AB) = c A \cdot dB
$$

### The Assembly

#### Register Map

The outlay of general-purpose registers is given in the map below, and was used to ensure neat code and minimal use of registers.

| r0 | r1 | r2 | r3 | r4 | r5 | r6 |
| - | - | - | - | - | - | - |
| $$$x_i$$$ | $$$a$$$ | $$$b$$$ | $$$c$$$ | $$$\lambda$$$ | misc. | misc. |

<br>

| r7 | r8 | r9 | r10 | r11 | r12 |
| - | - | - | - | - | - | - | - | - | - | - | - | - |
| misc. | misc. | misc. | misc. | $$$i$$$ | count_add | |



#### Defining constants

Constants were defined by using the `.word` and `.equ` directives.  The difference between the directives is that `.equ` defines a constant, whereas in the case of `.word`, space is allocated on the stack for the constant and initialized.

    @ Equates
        .equ    STEP,        2					@lambda at x10
        .equ    SCALE,      10
        .equ    SCALE2,    100
        .equ    LCOUNT,    0
        
    @ Define constant values here if necessary
        COUNT_ADD: .word COUNT

They are then loaded upon entering the routine:

        @ load constants
        ldr     r11, COUNT_ADD           @ global variable COUNT reference
        ldr     r10, =LCOUNT             @ initalize counting register
        ldr     r8, =SCALE               @ Store 10 in R8
        ldr     r9, =SCALE2              @ Store 100 in R9

#### Performing normal subroutine operations

A routine written in Assembly does not preserve the program's context by default.  The state of the registers (context), and the link register (containing the reference address just before the point of entry into the subroutine) needs to be saved prior to modification of the registers.  Failure to do so might result in a runtime error, for example a hard fault.  As such, we preserve the context by pushing it onto the heap, and pop it from the heap just before returning from the routine:

	push 	{r1-r12,lr} 			@ save the context

#### Computing $$$\nabla f(x)$$$

Computation of the derivative is given as follow.  Care was taken to preserve scaling at x100.

		@ 2ax + b at x100
        lsl     r5, r0, #1               @ (xi*100) * 2, Store in R5
        mul     r7, r2, r9               @ b * 100, Store in R7
        mla     r6, r1, r5, r7           @ a * [(xi*100) * 2] + [b * 100], Store in R6

A logical left shift `LSL` operation was used to multiply by 2, while `MLA` enabled two operations (multiplication and subsequently addition) to be performed, reducing the number of instructions written.  While it is noted that to fully accomodate 32-bit multiplication would require a 2 words (64-bit) space, a 32-bit space was assumed as subsequent iterations would require even larger spaces (e.g. 64-bit multiplication in iteration 2, 4 words).  Hence the upper 32-bits were discarded.


#### Computing the second expression

$$$x_{k+1}$$$ was computed in the second section.  As the formula involved $$$\lambda : \lambda = 0.2 \Rightarrow \lambda \leq 1$$$, and $$$\lambda$$$ (given by step) had to be scaled up by 10.  Hence, the total scaling was x1000.  Care was taken to preserve scaling at that value.  

The finalized version of $$$x_{k+1}$$$ was then renormalized to x100 by performing an `sdiv` operation.

        @ x_k+1 at x1000
        ldr     r4, =STEP                @ Store STEP * 10 in R4
        mul     r6, r6, r4               @ {a * [(xi*100) * 2] + [b * 100]} * (STEP*10), Store in R6

        mul     r7, r0, r8               @ (xi*100) * 10, Store in R7
        sub     r5, r7, r6               @ |(xi*100) * 10| – |{a * [(xi*100) * 2] + [b * 100]} * (STEP*10)|, Store in R5

        @ x_k+1 at x100
        sdiv    r6, r5, r8               @ \\\|(xi*100) * 10| – |{a * [(xi*100) * 2] + [b * 100]} * (STEP*10)|\\\ ÷ 10, Store in R6
                                         @ restore x_k+1 scaling to x100

#### Comparison until result is reached

The $$$ x_k $$$ and $$$x_{k+1}$$$ were then shifted into temporary locations.  $$$x_{k+1}$$$ was first shifted into `r0`, the return register, in anticipation of the event the loop guard was satisfied and exited.  The loop guard, $$$x_{k+1} = x_k$$$, was computed by doing a `bne` operation which triggered the status register `Z` flag, and subsequently a `bne` which would branch if they were not equal (i.e. the Z flag was not raised).  Iteration would continue until the loop condition was satisfied.

    compare:
        mov     r5, r0                   @ moving for temp comparison
        mov     r0, r6                   @ moving so it can loop back or return val, if needed
        cmp     r6, r5                   @ subtract and trigger sReg if zero
        bne     loop                     @ loop if not equal (Z flag checked) with r0 = x_k+1

#### Exiting

The number of iterations, stored in `r10`, was then written to the address pointed by `r11`, i.e. the global `COUNT`.  Context was then restored by popping the pushed registers on the stack, and doing an implicit branch to the return address of the link register given by the contents of the link register `lr`.  Note that in particular, the address of the link register (prior to subroutine entry) on the stack is instead pushed to the program counter, resuming execution.  Hence a `bx lr` instruction is not needed.

        str     r10, [r11]               @ write global varible COUNT

        pop     {r1-r12, pc}             @ restore the context

## Experiments & Discussion

#### λ Variation

**Examine the effect of changing the step size parameter λ on the number of iterations needed
to converge to the optimal value x* and the accuracy of the solution.
Note: this can be seen from the C program console window output**

| λ = 0.1 Full | λ = 0.5 Full | λ = 0.9 Full |
|-|-|-|
| <img src="http://joeltong.org/ee2024-lab-1/λ=0.1_Full.pdf.jpeg" style="width: 300px;"> | <img src="http://joeltong.org/ee2024-lab-1/λ=0.5_Full.pdf.jpeg" style="width: 300px;"> | <img src="http://joeltong.org/ee2024-lab-1/λ=0.9_Full.pdf.jpeg" style="width: 300px;"> |


It is apparent from the graphs above that there clearly exists a non-linear relationship between the step size $$$(λ)$$$ and the number of iterations $$$(n)$$$ executed. For all parameter values of the quadratic function $$$ ax^2 +bx+c $$$, there exists $$$λ_{max}$$$, such that the gradient decent algorithm never reaches a stable $$$x_{min}$$$ value due to the hinderance of physical computational limits. When $$$λ = 0.1$$$, as in the case above, the algorithm smoothly descends into the correct minimum value. As $$$λ$$$ is increased up until $$$0.5$$$, the number of iterations decrease because the 'changes' between $$$x_{k}$$$ and $$$x_{k+1}$$$ get larger and closer to the minimum value. But after $$$λ=0.5$$$, the 'changes' are so substantial that they sway back and forth between the two halves of the function. However, it is important to note that this is an isolated case with fixed parameters $$$a, b, c$$$. Different parameters will result in different 'pivots' like $$$λ=0.5$$$. 

#### ASM vs. C: Iteration

**Compare the number of iterations taken by the assembly language function with that of the
C program and explain your observations.
Note: this part is to be done without any assistance from your GA or tutor.**

<img src="http://joeltong.org/ee2024-lab-1/Step_vs._Iterations.pdf.jpeg" style="width: 600px;">

In most cases, the number of iterations performed by `optimize()` is smaller than that of the `main()` program. This is clearly evident in the case above for $$$ 0.1 ≤ λ ≤ 0.9 $$$. The effect is mainly a result of the difference in computational precision between the ASM and the C program. During the execution of numerical operations, `main()` is designed to handle floating point values (single precision: 32-bit). On the other hand, `optimize()` can only compute with scaled interger values, and thus has a limited ability to process smaller iterations between $$$x_{min}$$$ and $$$x_{0}$$$. Despite the differences in precision, the 'pivot' at $$$λ=0.5$$$ is still apparent in the C program due to the same reasons stated in the previous section. 


#### ASM vs. C: Accuracy

**Note: the solution for x which can be achieved using the assembly language
optimize() function may not be as precise as that which can be achieved through
gradient descent implemented in the C program. Compare these two solutions with the
true solution obtained through mathematical analysis in your report and discuss why
they are the same or different**


<img src="http://joeltong.org/ee2024-lab-1/ASM_Error_vs._C_Error.pdf.jpeg" style="width: 600px;">

The accuracy of all the solutions computed in the previous section was compared with the theoretically perfect $$$x_{min}$$$ value, i.e. $$$ \frac{-b}{2a} $$$. An interesting outcome of this experiment: higher number of iterations doesn't directly correlate to greater accuracy. In the case $$$a=1, b=-14, c=45$$$, the ASM solution is 100% accurate for all step sizes; the C program has negligibly small and yet non-zero errors. This is an isolated case where $$$x_{min}$$$ happens to be a whole number. If $$$a=4, b=-1, c=30$$$, where $$$x_{min}$$$ is a fraction, then the ASM solution is defective with 4% error, whereas the C solution has effectively 0% error. Generally, `optimize()` performs worse when $$$x_{min}$$$ is a fractional value or a number with many decimal places, due to the fact that the scaling function limits its decimal precision to only two places. When $$$x_{min}$$$ is a whole number, the accuracy of the individual decimal places doesn't matter, and often results in 0% error. 

### Limitations:


#### Algorithm

The gradient descent algorithm itself has a fundamental physical limitation. For both ASM and C implementations, there exist $$$λ_{a,max} $$$ and $$$  λ_{c,max} $$$ respectively, for which step sizes beyond these values will result in an infinite loop when executed on the ARM processor. This is either because the 'change' between $$$ x_{k} $$$ and $$$x_{k+1}$$$ during each iteration is so violent that a 32 bit register can no longer handle the value. Or as in the case $$$ a=7, b=-160, c=21, λ=0.2 $$$, the ASM program will be consumed by a state of instability, toggling between $$$11.42$$$ and $$$11.43$$$ for infinite iterations (decimal place accuracy limitation). Theoretically, if we were in the possession of a computer capable of infinite processing power, then it should be possible to calculate $$$x_{min}$$$ with 100% accuracy for any $$$a,b,c, λ $$$ in both ASM and C.

#### 32-bit Multiplication
While computing $$$\lambda \nabla f(x_k)$$$, the multiplication of 32-bit numbers lead to 64-bit results. The effects of this problem are negligible for results which constitute of 32-bits or less. Anything greater than 32-bits will be truncated, thus limiting the range of values for which the algorithm can perform properly.


#### ASM $$$x_{0}$$$ precision
The algorithm's scaling function can only take in $$$x_{0}$$$ values with a maximum of 2 decimal points. Any decimal places after that will be truncated and ignored by `optimize()`. *Note:* The decimal precision of the ASM function can be increased by changing the scaling values. For example, instead of scaling the algorithm by 100, if we scale it by 1000, the solution $$$x_{min}$$$ will be evaluated to one more decimal place. 


## Statement of Contribution

| Student 1 |  Student 2: |
| - | - |
| Joel Haowen **Tong** (A0108165J) | Mohit **Shridhar** (A0105912N) |

**A. Joint Work in algorithm development, programming and report writing (briefly list a few specific aspects):**

Main algorithm execution/development, ASM debugging, experimentation

**B. Student 1’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):**

Main algorithm design, ASM & C debugging, Report: Background, Code

**C. Student 2’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):**

Bonus question, supplementary functions in C, report: Experiments & Discussion

**We both agree that the statements above are truthful and accurate.**

**Signatures and Names:**

<br>

| Joel Haowen **Tong** (A0108165J) | Mohit **Shridhar** (A0105912N) |
| - | - |
| <br><br><br> | <br><br><br> |

## Appendix 0: The Code

**main.c**
	
    /** main.c **/
    
	#include "stdio.h"
    #include "math.h"
    
    // EE2024 Assignment 1
    
    /**
        @ Authors:
        @ TONG Haowen Joel <me at joeltong dot org>
        @ SHRIDHAR Mohit <mohit at nus dot edu dot sg>
    */
    
    // Optimization routine written in assembly language
    // Input parameters (signed integers):
    //     xi : integer version of x
    //     a, b, c : integer coefficients of f(x)
    // Output : returns solution x (signed integer)
    
    unsigned int COUNT; // global variable passed to ASM
    extern int optimize(int xi, int a, int b, int c);
    
    
    int main(void)
    {
        int a=1, b=-14, c=45, xsoli; // Default a, b, c
        float fp, x, xprev, xsol, change, lambda=0.2, perfect;
    
        printf("Enter the parameters of the quadratic function (a[x^2] + b[x] + c) to be optimized\n");
    
        printf("Int a: ");
        scanf("%d", &a);
    
        printf("Int b: ");
        scanf("%d", &b);
    
        printf("Int c: ");
        scanf("%d", &c);
    
        printf("Initial value of x (press [Enter] after keying in): "); // eg: try x0 = -12.35
        scanf("%f", &x);
    
    //  ARM ASM & Integer version
    
        printf("ARM ASM & Integer version:\n");
        xsoli = (int) optimize((int) (x * 100),a,b,c);
        xsol = ( (float) xsoli ) / (100.0);
        printf("Iterative ASM solution : %f (%d iterations)\n",xsol, COUNT);
    
        perfect = (b*-1.0)/(2.0*a);
        printf("Theoretical solution: %f\n\n", perfect);
    
    //  C & Floating Point version
    
        printf("C & Floating point version:\n",x);
        int countC = 0;
        while (1)
        {
            countC++;
    
            fp = 2*a*x + b;
    
            xprev = x;
            change = -lambda*fp;
            x += change;
    
            printf("x: %f, fp: %f, change: %f\n", x, fp, change);
            if (x==xprev) {
                printf("Floating point C Iteration: %f\n", x);
                break;
            }
        }
    
        printf("\nResult: ASM: %f (%d iterations), C floating: %f (%d iterations), Theoretical: %f\n", xsol, COUNT, x, countC, perfect);
    
        // Calculate error:
        float errorAsm = (fabs(perfect-xsol) / (perfect * 1.0)) * 100.0;
        float errorC = (fabs(perfect-x) / (perfect * 1.0)) * 100.0;
        printf("ASM Error: %f%%, C-floating Error: %f%%\n", errorAsm, errorC);
    
        // Enter an infinite loop, just incrementing a counter
        // This is for convenience to allow registers, variables and memory locations to be inspected at the end
        volatile static int loop = 0;
        while (1) {
            loop++;
        }
    }

**optimize.s**

         .syntax unified
         .cpu cortex-m3
         .thumb
         .align 2
         .global    optimize
         .global COUNT                       @ global variable
         .thumb_func
    
    
    @ EE2024 Assignment 1
    @ Authors:
    @ TONG Haowen Joel <me at joeltong dot org>
    @ SHRIDHAR Mohit <mohit at nus dot edu dot sg>
    
    @ Equates
        .equ     STEP,      2                @lambda at x10
        .equ     SCALE,     10
        .equ     SCALE2,    100
        .equ     LCOUNT,    0
    
    
    optimize:
        @ TODO: THIS VERSION DOES NOT CONSIDER EXCEPTIONS
    
        @ General Purpose Register Map
        @ r0:         [xk*100], return value
        @ r1:         a
        @ r2:         b
        @ r3:         c
    
        @ r8:         SCALE 10
        @ r9:         SCALE 100
    
    
        preamble:
            push     {r1-r12,lr}             @ save the context
    
            @ load constants
            ldr     r11, COUNT_ADD           @ global variable COUNT reference
            ldr     r10, =LCOUNT             @ initalize counting register
            ldr     r8, =SCALE               @ Store 10 in R8
            ldr     r9, =SCALE2              @ Store 100 in R9
    
        loop:
            add     r10, r10, #1             @ increment counter
    
            @ 2ax + b at x100
            lsl     r5, r0, #1               @ (xk*100) * 2, Store in R5
            mul     r7, r2, r9               @ b * 100, Store in R7
            mla     r5, r1, r5, r7           @ a * [(xk*100) * 2] + [b * 100], Store in R5
    
            @ x_k+1 at x1000
            ldr     r4, =STEP                @ Store STEP * 10 in R4
            mul     r5, r5, r4               @ {a * [(xk*100) * 2] + [b * 100]} * (STEP*10), Store in R6
    
            mul     r6, r0, r8               @ (xk*100) * 10, Store in R7
            sub     r5, r6, r5               @ |(xk*100) * 10| – |{a * [(xk*100) * 2] + [b * 100]} * (STEP*10)|, Store in R5
    
            @ x_k+1 at x100
            sdiv    r6, r5, r8               @ \\\|(xk*100) * 10| – |{a * [(xk*100) * 2] + [b * 100]} * (STEP*10)|\\\ ÷ 10, Store in R6
                                             @ restore x_k+1 scaling to x100
    
        compare:
            mov     r5, r0                   @ moving for temp comparison
            mov     r0, r6                   @ moving so it can loop back or return val, if needed
            cmp     r6, r5                   @ subtract and trigger sReg if zero
            bne     loop                     @ loop if not equal (Z flag checked) with r0 = x_k+1
    
            str     r10, [r11]               @ write global varible COUNT
    
            pop     {r1-r12, pc}             @ restore the context
    
    @ Define constant values here if necessary
    COUNT_ADD: .word COUNT

## Appendix 1: Supplementary Graphs

<img src="http://joeltong.org/ee2024-lab-1/Graphs%20Extra%20copy.jpg" style="width:100%;" />
