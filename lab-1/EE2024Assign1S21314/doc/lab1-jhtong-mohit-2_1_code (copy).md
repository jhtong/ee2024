#### Comparison until result is reached

The $$$ x_k $$$ and $$$x_{k+1}$$$ were then shifted into temporary locations.  $$$x_{k+1}$$$ was first shifted into `r0`, the return register, in anticipation of the event the loop guard was satisfied and exited.  The loop guard, $$$x_{k+1} = x_k$$$, was computed by doing a `bne` operation which triggered the status register `Z` flag, and subsequently a `bne` which would branch if they were not equal (i.e. the Z flag was not raised).  Iteration would continue until the loop condition was satisfied.

    compare:
        mov     r5, r0                   @ moving for temp comparison
        mov     r0, r6                   @ moving so it can loop back or return val, if needed
        cmp     r6, r5                   @ subtract and trigger sReg if zero
        bne     loop                     @ loop if not equal (Z flag checked) with r0 = x_k+1

#### Exiting

The number of iterations, stored in `r10`, was then written to the address pointed by `r11`, i.e. the global `COUNT`.  Context was then restored by popping the pushed registers on the stack, and doing an implicit branch to the return address of the link register given by the contents of the link register `lr`.  Note that in particular, the address of the link register (prior to subroutine entry) on the stack is instead pushed to the program counter, resuming execution.  Hence a `bx lr` instruction is not needed.

        str     r10, [r11]               @ write global varible COUNT

        pop     {r1-r12, pc}             @ restore the context
