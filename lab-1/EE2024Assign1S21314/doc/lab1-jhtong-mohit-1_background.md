# EE2024 Assignment 1

| Name | Matric Number |
| -- | -- |
| Joel Haowen **Tong** | A0108165J |
| Mohit **Shridhar** | A0105912N |

## Objective

This report explores the Gradient Descent algorithm to find the minimum for the polynomial $$$ax^2 + bx + c$$$, using both ARM assembly and C.

### Background

The Gradient Descent algorithm finds the minimum of a given function.  It has a complexity of O(n).

If a given function $$$F(x)$$$ is well-defined and differentiable within a neighborhood that encloses the minimum point $$$x_{min}$$$ and $$$x_0$$$, then one should travel in the direction opposite the gradeint to reach the minimum point $$$x_{min}$$$, specifically:

$$
F(x_{k+1}) = F(x_k) - \lambda \nabla F(x) : k \in \mathbb{N}
$$

For sufficiently small values of $$$\lambda < 1$$$, 

$$
F(x_{k+1}) \leq F(x)
$$

The local minimum is given by the terminating condition:

$$
F(x_{n+1}) \approx F(x_n) : \exists \, n : x_{n+1} = x_{min} \bullet n  \in \mathbb{N}
$$

An example of the algorithm in operation is given by:

<img src="http://upload.wikimedia.org/wikipedia/commons/6/60/Banana-SteepDesc.gif" style="width: 300px;"  />