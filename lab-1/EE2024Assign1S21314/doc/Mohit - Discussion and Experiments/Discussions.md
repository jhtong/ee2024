## Experiments & Discussion

#### λ Variation

**Place graphs "λ = 0.1 Full", "λ = 0.5 Full", "λ = 0.9 Full" here** **3 in a ROW**

It is apparent from the graphs above that there clearly exists a non-linear relationship between the step size $$$(λ)$$$ and the number of iterations $$$(n)$$$ executed. For all parameter values of the quadratic function $$$ ax^2 +bx+c $$$, there exists $$$λ_{max}$$$, such that the gradient decent algorithm never reaches a stable $$$x_{min}$$$ value due to the hinderance of physical computational limits. When $$$λ = 0.1$$$, as in the case above, the algorithm smoothly descends into the correct minimum value. As $$$λ$$$ is increased up until $$$0.5$$$, the number of iterations decrease because the 'changes' between $$$x_{k}$$$ and $$$x_{k+1}$$$ get larger and closer to the minimum value. But after $$$λ=0.5$$$, the 'changes' are so substantial that they sway back and forth between the two halves of the function. However, it is important to note that this is an isolated case with fixed parameters $$$a, b, c$$$. Different parameters will result in different 'pivots' like $$$λ=0.5$$$. 

#### ASM vs. C: Iteration
**Place graph "Step vs. Iterations" here**

In most cases, the number of iterations performed by `optimize()` is smaller than that of the `main()` program. This is clearly evident in the case above for $$$ 0.1 ≤ λ ≤ 0.9 $$$. The effect is mainly a result of the difference in computational precision between the ASM and the C program. During the execution of numerical operations, `main()` is designed to handle floating point values (single precision: 32-bit). On the other hand, `optimize()` can only compute with scaled interger values, and thus has a limited ability to process smaller iterations between $$$x_{min}$$$ and $$$x_{0}$$$. Despite the differences in precision, the 'pivot' at $$$λ=0.5$$$ is still apparent in the C program due to the same reasons stated in the previous section. 


#### ASM vs. C: Accuracy

**Place graphs "ASM Error vs. C Error" here**

The accuracy of all the solutions computed in the previous section was compared with the theoretically perfect $$$x_{min}$$$ value, i.e. $$$ \frac{-b}{2a} $$$. An interesting outcome of this experiment: higher number of iterations doesn't directly correlate to greater accuracy. In the case $$$a=1, b=-14, c=45$$$, the ASM solution is 100% accurate for all step sizes; the C program has negligibly small and yet non-zero errors. This is an isolated case where $$$x_{min}$$$ happens to be a whole number. If $$$a=4, b=-1, c=30$$$, where $$$x_{min}$$$ is a fraction, then the ASM solution is defective with 4% error, whereas the C solution has effectively 0% error. Generally, `optimize()` performs worse when $$$x_{min}$$$ is a fractional value or a number with many decimal places, due to the fact that the scaling function limits its decimal precision to only two places. When $$$x_{min}$$$ is a whole number, the accuracy of the individual decimal places doesn't matter, and often results in 0% error. 

### Limitations:


#### Algorithm

The gradient descent algorithm itself has a fundamental physical limitation. For both ASM and C implementations, there exist $$$λ_{a,max} $$$ and $$$  λ_{c,max} $$$ respectively, for which step sizes beyond these values will result in an infinite loop when executed on the ARM processor. This is either because the 'change' between $$$ x_{k} $$$ and $$$x_{k+1}$$$ during each iteration is so violent that a 32 bit register can no longer handle the value. Or as in the case $$$ a=7, b=-160, c=21, λ=0.2 $$$, the ASM program will be consumed by a state of instability, toggling between $$$11.42$$$ and $$$11.43$$$ for infinite iterations (decimal place accuracy limitation). Theoretically, if we were in the possession of a computer capable of infinite processing power, then it should be possible to calculate $$$x_{min}$$$ with 100% accuracy for any $$$a,b,c, λ $$$ in both ASM and C.

#### 32-bit Multiplication
While computing $$$\lambda \nabla f(x_k)$$$, the multiplication of 32-bit numbers lead to 64-bit results. The effects of this problem are negligible for results which constitute of 32-bits or less. Anything greater than 32-bits will be truncated, thus limiting the range of values for which the algorithm can perform properly.


#### ASM $$$x_{0}$$$ precision
The algorithm's scaling function can only take in $$$x_{0}$$$ values with a maximum of 2 decimal points. Any decimal places after that will be truncated and ignored by `optimize()`. *Note:* The decimal precision of the ASM function can be increased by changing the scaling values. For example, instead of scaling the algorithm by 100, if we scale it by 1000, the solution $$$x_{min}$$$ will be evaluated to one more decimal place. 


## Statement of Contribution

| Student 1 |  Student 2: |
| - | - |
| Joel Haowen **Tong** (A0108165J) | Mohit **Shridhar** (A0105912N) |

**A. Joint Work in algorithm development, programming and report writing (briefly list a few specific aspects):**
Main algorithm execution/development, ASM debugging, experimentation

**B. Student 1’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):**
Main algorithm design, ASM & C debugging, Report: Background, Code

**C. Student 2’s Work in algorithm development, programming and report writing (briefly list a few specific aspects):**
Bonus question, supplementary functions in C, report: Experiments & Discussion

**We both agree that the statements above are truthful and accurate.**

**Signatures and Names:**

<br>

--------------------------


## Appendix
**Insert other graphs**