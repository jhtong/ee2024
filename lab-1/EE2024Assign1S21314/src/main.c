#include "stdio.h"
#include "math.h"

// EE2024 Assignment 1

/**
	@ Authors:
	@ TONG Haowen Joel <me at joeltong dot org>
	@ SHRIDHAR Mohit <mohit at nus dot edu dot sg>
*/

// Optimization routine written in assembly language
// Input parameters (signed integers):
//     xi : integer version of x
//     a, b, c : integer coefficients of f(x)
// Output : returns solution x (signed integer)

unsigned int COUNT; // global variable passed to ASM
extern int optimize(int xi, int a, int b, int c);


int main(void)
{
	int a=1, b=-14, c=45, xsoli; // Default a, b, c
	float fp, x, xprev, xsol, change, lambda=0.2, perfect;

	printf("Enter the parameters of the quadratic function (a[x^2] + b[x] + c) to be optimized\n");

	printf("Int a: ");
    scanf("%d", &a);

    printf("Int b: ");
    scanf("%d", &b);

    printf("Int c: ");
    scanf("%d", &c);

    printf("Initial value of x (press [Enter] after keying in): "); // eg: try x0 = -12.35
    scanf("%f", &x);

//  ARM ASM & Integer version

    printf("ARM ASM & Integer version:\n");
    xsoli = (int) optimize((int) (x * 100),a,b,c);
    xsol = ( (float) xsoli ) / (100.0);
    printf("Iterative ASM solution : %f (%d iterations)\n",xsol, COUNT);

    perfect = (b*-1.0)/(2.0*a);
    printf("Theoretical solution: %f\n\n", perfect);

//  C & Floating Point version

  	printf("C & Floating point version:\n",x);
  	int countC = 0;
    while (1)
    {
    	countC++;

    	fp = 2*a*x + b;

    	xprev = x;
    	change = -lambda*fp;
    	x += change;

    	printf("x: %f, fp: %f, change: %f\n", x, fp, change);
    	if (x==xprev) {
    		printf("Floating point C Iteration: %f\n", x);
    		break;
    	}
    }

    printf("\nResult: ASM: %f (%d iterations), C floating: %f (%d iterations), Theoretical: %f\n", xsol, COUNT, x, countC, perfect);

    // Calculate error:
    float errorAsm = (fabs(perfect-xsol) / (perfect * 1.0)) * 100.0;
    float errorC = (fabs(perfect-x) / (perfect * 1.0)) * 100.0;
    printf("ASM Error: %f%%, C-floating Error: %f%%\n", errorAsm, errorC);

    // Enter an infinite loop, just incrementing a counter
	// This is for convenience to allow registers, variables and memory locations to be inspected at the end
	volatile static int loop = 0;
	while (1) {
		loop++;
	}
}
