   1              		.syntax unified
   2              		.cpu cortex-m3
   3              		.fpu softvfp
   4              		.eabi_attribute 20, 1
   5              		.eabi_attribute 21, 1
   6              		.eabi_attribute 23, 3
   7              		.eabi_attribute 24, 1
   8              		.eabi_attribute 25, 1
   9              		.eabi_attribute 26, 1
  10              		.eabi_attribute 30, 6
  11              		.eabi_attribute 34, 1
  12              		.eabi_attribute 18, 4
  13              		.thumb
  14              		.file	"main.c"
  15              		.text
  16              	.Ltext0:
  17              		.cfi_sections	.debug_frame
  18              		.comm	COUNT,4,4
  19              		.section	.rodata
  20              		.align	2
  21              	.LC0:
  22 0000 456E7465 		.ascii	"Enter the parameters of the quadratic function (a[x"
  22      72207468 
  22      65207061 
  22      72616D65 
  22      74657273 
  23 0033 5E325D20 		.ascii	"^2] + b[x] + c) to be optimized\012\000"
  23      2B20625B 
  23      785D202B 
  23      20632920 
  23      746F2062 
  24              		.align	2
  25              	.LC1:
  26 0054 496E7420 		.ascii	"Int a: \000"
  26      613A2000 
  27              		.align	2
  28              	.LC2:
  29 005c 256400   		.ascii	"%d\000"
  30 005f 00       		.align	2
  31              	.LC3:
  32 0060 496E7420 		.ascii	"Int b: \000"
  32      623A2000 
  33              		.align	2
  34              	.LC4:
  35 0068 496E7420 		.ascii	"Int c: \000"
  35      633A2000 
  36              		.align	2
  37              	.LC5:
  38 0070 496E6974 		.ascii	"Initial value of x (press [Enter] after keying in):"
  38      69616C20 
  38      76616C75 
  38      65206F66 
  38      20782028 
  39 00a3 2000     		.ascii	" \000"
  40 00a5 000000   		.align	2
  41              	.LC6:
  42 00a8 256600   		.ascii	"%f\000"
  43 00ab 00       		.align	2
  44              	.LC7:
  45 00ac 41524D20 		.ascii	"ARM ASM & Integer version:\012\000"
  45      41534D20 
  45      2620496E 
  45      74656765 
  45      72207665 
  46              		.global	__aeabi_fmul
  47              		.global	__aeabi_f2iz
  48              		.global	__aeabi_i2f
  49              		.global	__aeabi_fdiv
  50              		.global	__aeabi_f2d
  51              		.align	2
  52              	.LC8:
  53 00c8 49746572 		.ascii	"Iterative ASM solution : %f (%d iterations)\012\000"
  53      61746976 
  53      65204153 
  53      4D20736F 
  53      6C757469 
  54              		.global	__aeabi_i2d
  55              		.global	__aeabi_dadd
  56              		.global	__aeabi_ddiv
  57              		.global	__aeabi_d2f
  58 00f5 000000   		.align	2
  59              	.LC9:
  60 00f8 5468656F 		.ascii	"Theoretical solution: %f\012\012\000"
  60      72657469 
  60      63616C20 
  60      736F6C75 
  60      74696F6E 
  61 0113 00       		.align	2
  62              	.LC10:
  63 0114 43202620 		.ascii	"C & Floating point version:\012\000"
  63      466C6F61 
  63      74696E67 
  63      20706F69 
  63      6E742076 
  64              		.global	__aeabi_fadd
  65 0131 000000   		.align	2
  66              	.LC11:
  67 0134 783A2025 		.ascii	"x: %f, fp: %f, change: %f\012\000"
  67      662C2066 
  67      703A2025 
  67      662C2063 
  67      68616E67 
  68              		.global	__aeabi_fcmpeq
  69 014f 00       		.align	2
  70              	.LC12:
  71 0150 466C6F61 		.ascii	"Floating point C Iteration: %f\012\000"
  71      74696E67 
  71      20706F69 
  71      6E742043 
  71      20497465 
  72              		.align	2
  73              	.LC13:
  74 0170 0A526573 		.ascii	"\012Result: ASM: %f (%d iterations), C floating: %f"
  74      756C743A 
  74      2041534D 
  74      3A202566 
  74      20282564 
  75 01a0 20282564 		.ascii	" (%d iterations), Theoretical: %f\012\000"
  75      20697465 
  75      72617469 
  75      6F6E7329 
  75      2C205468 
  76              		.global	__aeabi_fsub
  77              		.global	__aeabi_dmul
  78 01c3 00       		.align	2
  79              	.LC14:
  80 01c4 41534D20 		.ascii	"ASM Error: %f%%, C-floating Error: %f%%\012\000"
  80      4572726F 
  80      723A2025 
  80      6625252C 
  80      20432D66 
  81 01ed 000000   		.section	.text.main,"ax",%progbits
  82              		.align	2
  83              		.global	main
  84              		.thumb
  85              		.thumb_func
  87              	main:
  88              	.LFB0:
  89              		.file 1 "../src/main.c"
   1:../src/main.c **** #include "stdio.h"
   2:../src/main.c **** #include "math.h"
   3:../src/main.c **** 
   4:../src/main.c **** // EE2024 Assignment 1
   5:../src/main.c **** 
   6:../src/main.c **** /**
   7:../src/main.c **** 	@ Authors:
   8:../src/main.c **** 	@ TONG Haowen Joel <me at joeltong dot org>
   9:../src/main.c **** 	@ SHRIDHAR Mohit <mohit at nus dot edu dot sg>
  10:../src/main.c **** */
  11:../src/main.c **** 
  12:../src/main.c **** // Optimization routine written in assembly language
  13:../src/main.c **** // Input parameters (signed integers):
  14:../src/main.c **** //     xi : integer version of x
  15:../src/main.c **** //     a, b, c : integer coefficients of f(x)
  16:../src/main.c **** // Output : returns solution x (signed integer)
  17:../src/main.c **** 
  18:../src/main.c **** unsigned int COUNT; // global variable passed to ASM
  19:../src/main.c **** extern int optimize(int xi, int a, int b, int c);
  20:../src/main.c **** 
  21:../src/main.c **** 
  22:../src/main.c **** int main(void)
  23:../src/main.c **** {
  90              		.loc 1 23 0
  91              		.cfi_startproc
  92              		@ args = 0, pretend = 0, frame = 56
  93              		@ frame_needed = 1, uses_anonymous_args = 0
  94 0000 2DE9F043 		push	{r4, r5, r6, r7, r8, r9, lr}
  95              		.cfi_def_cfa_offset 28
  96              		.cfi_offset 4, -28
  97              		.cfi_offset 5, -24
  98              		.cfi_offset 6, -20
  99              		.cfi_offset 7, -16
 100              		.cfi_offset 8, -12
 101              		.cfi_offset 9, -8
 102              		.cfi_offset 14, -4
 103 0004 97B0     		sub	sp, sp, #92
 104              		.cfi_def_cfa_offset 120
 105 0006 08AF     		add	r7, sp, #32
 106              		.cfi_def_cfa 7, 88
  24:../src/main.c **** 	int a=1, b=-14, c=45, xsoli; // Default a, b, c
 107              		.loc 1 24 0
 108 0008 0123     		movs	r3, #1
 109 000a FB60     		str	r3, [r7, #12]
 110 000c 6FF00D03 		mvn	r3, #13
 111 0010 BB60     		str	r3, [r7, #8]
 112 0012 2D23     		movs	r3, #45
 113 0014 7B60     		str	r3, [r7, #4]
  25:../src/main.c **** 	float fp, x, xprev, xsol, change, lambda=0.2, perfect;
 114              		.loc 1 25 0
 115 0016 684B     		ldr	r3, .L8
 116 0018 3B63     		str	r3, [r7, #48]	@ float
  26:../src/main.c **** 
  27:../src/main.c **** 	printf("Enter the parameters of the quadratic function (a[x^2] + b[x] + c) to be optimized\n");
 117              		.loc 1 27 0
 118 001a 6848     		ldr	r0, .L8+4
 119 001c FFF7FEFF 		bl	printf
  28:../src/main.c **** 
  29:../src/main.c **** 	printf("Int a: ");
 120              		.loc 1 29 0
 121 0020 6748     		ldr	r0, .L8+8
 122 0022 FFF7FEFF 		bl	printf
  30:../src/main.c ****     scanf("%d", &a);
 123              		.loc 1 30 0
 124 0026 07F10C03 		add	r3, r7, #12
 125 002a 6648     		ldr	r0, .L8+12
 126 002c 1946     		mov	r1, r3
 127 002e FFF7FEFF 		bl	scanf
  31:../src/main.c **** 
  32:../src/main.c ****     printf("Int b: ");
 128              		.loc 1 32 0
 129 0032 6548     		ldr	r0, .L8+16
 130 0034 FFF7FEFF 		bl	printf
  33:../src/main.c ****     scanf("%d", &b);
 131              		.loc 1 33 0
 132 0038 07F10803 		add	r3, r7, #8
 133 003c 6148     		ldr	r0, .L8+12
 134 003e 1946     		mov	r1, r3
 135 0040 FFF7FEFF 		bl	scanf
  34:../src/main.c **** 
  35:../src/main.c ****     printf("Int c: ");
 136              		.loc 1 35 0
 137 0044 6148     		ldr	r0, .L8+20
 138 0046 FFF7FEFF 		bl	printf
  36:../src/main.c ****     scanf("%d", &c);
 139              		.loc 1 36 0
 140 004a 3B1D     		adds	r3, r7, #4
 141 004c 5D48     		ldr	r0, .L8+12
 142 004e 1946     		mov	r1, r3
 143 0050 FFF7FEFF 		bl	scanf
  37:../src/main.c **** 
  38:../src/main.c ****     printf("Initial value of x (press [Enter] after keying in): "); // eg: try x0 = -12.35
 144              		.loc 1 38 0
 145 0054 5E48     		ldr	r0, .L8+24
 146 0056 FFF7FEFF 		bl	printf
  39:../src/main.c ****     scanf("%f", &x);
 147              		.loc 1 39 0
 148 005a 3B46     		mov	r3, r7
 149 005c 5D48     		ldr	r0, .L8+28
 150 005e 1946     		mov	r1, r3
 151 0060 FFF7FEFF 		bl	scanf
  40:../src/main.c **** 
  41:../src/main.c **** //  ARM ASM & Integer version
  42:../src/main.c **** 
  43:../src/main.c ****     printf("ARM ASM & Integer version:\n");
 152              		.loc 1 43 0
 153 0064 5C48     		ldr	r0, .L8+32
 154 0066 FFF7FEFF 		bl	printf
  44:../src/main.c ****     xsoli = (int) optimize((int) (x * 100),a,b,c);
 155              		.loc 1 44 0
 156 006a 3B68     		ldr	r3, [r7]	@ float
 157 006c 1846     		mov	r0, r3
 158 006e 5B49     		ldr	r1, .L8+36
 159 0070 FFF7FEFF 		bl	__aeabi_fmul
 160 0074 0346     		mov	r3, r0
 161 0076 1846     		mov	r0, r3
 162 0078 FFF7FEFF 		bl	__aeabi_f2iz
 163 007c F968     		ldr	r1, [r7, #12]
 164 007e BA68     		ldr	r2, [r7, #8]
 165 0080 7B68     		ldr	r3, [r7, #4]
 166 0082 FFF7FEFF 		bl	optimize
 167 0086 F862     		str	r0, [r7, #44]
  45:../src/main.c ****     xsol = ( (float) xsoli ) / (100.0);
 168              		.loc 1 45 0
 169 0088 F86A     		ldr	r0, [r7, #44]
 170 008a FFF7FEFF 		bl	__aeabi_i2f
 171 008e 0346     		mov	r3, r0
 172 0090 1846     		mov	r0, r3
 173 0092 5249     		ldr	r1, .L8+36
 174 0094 FFF7FEFF 		bl	__aeabi_fdiv
 175 0098 0346     		mov	r3, r0
 176 009a BB62     		str	r3, [r7, #40]	@ float
  46:../src/main.c ****     printf("Iterative ASM solution : %f (%d iterations)\n",xsol, COUNT);
 177              		.loc 1 46 0
 178 009c B86A     		ldr	r0, [r7, #40]	@ float
 179 009e FFF7FEFF 		bl	__aeabi_f2d
 180 00a2 0246     		mov	r2, r0
 181 00a4 0B46     		mov	r3, r1
 182 00a6 4E49     		ldr	r1, .L8+40
 183 00a8 0968     		ldr	r1, [r1]
 184 00aa 0091     		str	r1, [sp]
 185 00ac 4D48     		ldr	r0, .L8+44
 186 00ae FFF7FEFF 		bl	printf
  47:../src/main.c **** 
  48:../src/main.c ****     perfect = (b*-1.0)/(2.0*a);
 187              		.loc 1 48 0
 188 00b2 BB68     		ldr	r3, [r7, #8]
 189 00b4 1846     		mov	r0, r3
 190 00b6 FFF7FEFF 		bl	__aeabi_i2d
 191 00ba 0246     		mov	r2, r0
 192 00bc 0B46     		mov	r3, r1
 193 00be 1446     		mov	r4, r2
 194 00c0 83F00045 		eor	r5, r3, #-2147483648
 195 00c4 FB68     		ldr	r3, [r7, #12]
 196 00c6 1846     		mov	r0, r3
 197 00c8 FFF7FEFF 		bl	__aeabi_i2d
 198 00cc 0246     		mov	r2, r0
 199 00ce 0B46     		mov	r3, r1
 200 00d0 1046     		mov	r0, r2
 201 00d2 1946     		mov	r1, r3
 202 00d4 FFF7FEFF 		bl	__aeabi_dadd
 203 00d8 0246     		mov	r2, r0
 204 00da 0B46     		mov	r3, r1
 205 00dc 2046     		mov	r0, r4
 206 00de 2946     		mov	r1, r5
 207 00e0 FFF7FEFF 		bl	__aeabi_ddiv
 208 00e4 0246     		mov	r2, r0
 209 00e6 0B46     		mov	r3, r1
 210 00e8 1046     		mov	r0, r2
 211 00ea 1946     		mov	r1, r3
 212 00ec FFF7FEFF 		bl	__aeabi_d2f
 213 00f0 0346     		mov	r3, r0
 214 00f2 7B62     		str	r3, [r7, #36]	@ float
  49:../src/main.c ****     printf("Theoretical solution: %f\n\n", perfect);
 215              		.loc 1 49 0
 216 00f4 786A     		ldr	r0, [r7, #36]	@ float
 217 00f6 FFF7FEFF 		bl	__aeabi_f2d
 218 00fa 0246     		mov	r2, r0
 219 00fc 0B46     		mov	r3, r1
 220 00fe 3A48     		ldr	r0, .L8+48
 221 0100 FFF7FEFF 		bl	printf
  50:../src/main.c **** 
  51:../src/main.c **** //  C & Floating Point version
  52:../src/main.c **** 
  53:../src/main.c ****   	printf("C & Floating point version:\n",x);
 222              		.loc 1 53 0
 223 0104 3B68     		ldr	r3, [r7]	@ float
 224 0106 1846     		mov	r0, r3
 225 0108 FFF7FEFF 		bl	__aeabi_f2d
 226 010c 0246     		mov	r2, r0
 227 010e 0B46     		mov	r3, r1
 228 0110 3648     		ldr	r0, .L8+52
 229 0112 FFF7FEFF 		bl	printf
  54:../src/main.c ****   	int countC = 0;
 230              		.loc 1 54 0
 231 0116 0023     		movs	r3, #0
 232 0118 7B63     		str	r3, [r7, #52]
 233              	.L5:
  55:../src/main.c ****     while (1)
  56:../src/main.c ****     {
  57:../src/main.c ****     	countC++;
 234              		.loc 1 57 0
 235 011a 7B6B     		ldr	r3, [r7, #52]
 236 011c 0133     		adds	r3, r3, #1
 237 011e 7B63     		str	r3, [r7, #52]
  58:../src/main.c **** 
  59:../src/main.c ****     	fp = 2*a*x + b;
 238              		.loc 1 59 0
 239 0120 FB68     		ldr	r3, [r7, #12]
 240 0122 5B00     		lsls	r3, r3, #1
 241 0124 1846     		mov	r0, r3
 242 0126 FFF7FEFF 		bl	__aeabi_i2f
 243 012a 0246     		mov	r2, r0
 244 012c 3B68     		ldr	r3, [r7]	@ float
 245 012e 1046     		mov	r0, r2
 246 0130 1946     		mov	r1, r3
 247 0132 FFF7FEFF 		bl	__aeabi_fmul
 248 0136 0346     		mov	r3, r0
 249 0138 1C46     		mov	r4, r3
 250 013a BB68     		ldr	r3, [r7, #8]
 251 013c 1846     		mov	r0, r3
 252 013e FFF7FEFF 		bl	__aeabi_i2f
 253 0142 0346     		mov	r3, r0
 254 0144 2046     		mov	r0, r4
 255 0146 1946     		mov	r1, r3
 256 0148 FFF7FEFF 		bl	__aeabi_fadd
 257 014c 0346     		mov	r3, r0
 258 014e 3B62     		str	r3, [r7, #32]	@ float
  60:../src/main.c **** 
  61:../src/main.c ****     	xprev = x;
 259              		.loc 1 61 0
 260 0150 3B68     		ldr	r3, [r7]	@ float
 261 0152 FB61     		str	r3, [r7, #28]	@ float
  62:../src/main.c ****     	change = -lambda*fp;
 262              		.loc 1 62 0
 263 0154 3B6B     		ldr	r3, [r7, #48]
 264 0156 83F00043 		eor	r3, r3, #-2147483648
 265 015a 1846     		mov	r0, r3
 266 015c 396A     		ldr	r1, [r7, #32]	@ float
 267 015e FFF7FEFF 		bl	__aeabi_fmul
 268 0162 0346     		mov	r3, r0
 269 0164 BB61     		str	r3, [r7, #24]	@ float
  63:../src/main.c ****     	x += change;
 270              		.loc 1 63 0
 271 0166 3B68     		ldr	r3, [r7]	@ float
 272 0168 1846     		mov	r0, r3
 273 016a B969     		ldr	r1, [r7, #24]	@ float
 274 016c FFF7FEFF 		bl	__aeabi_fadd
 275 0170 0346     		mov	r3, r0
 276 0172 3B60     		str	r3, [r7]	@ float
  64:../src/main.c **** 
  65:../src/main.c ****     	printf("x: %f, fp: %f, change: %f\n", x, fp, change);
 277              		.loc 1 65 0
 278 0174 3B68     		ldr	r3, [r7]	@ float
 279 0176 1846     		mov	r0, r3
 280 0178 FFF7FEFF 		bl	__aeabi_f2d
 281 017c 0446     		mov	r4, r0
 282 017e 0D46     		mov	r5, r1
 283 0180 386A     		ldr	r0, [r7, #32]	@ float
 284 0182 FFF7FEFF 		bl	__aeabi_f2d
 285 0186 8046     		mov	r8, r0
 286 0188 8946     		mov	r9, r1
 287 018a B869     		ldr	r0, [r7, #24]	@ float
 288 018c FFF7FEFF 		bl	__aeabi_f2d
 289 0190 0246     		mov	r2, r0
 290 0192 0B46     		mov	r3, r1
 291 0194 CDE90089 		strd	r8, [sp]
 292 0198 CDE90223 		strd	r2, [sp, #8]
 293 019c 1448     		ldr	r0, .L8+56
 294 019e 2246     		mov	r2, r4
 295 01a0 2B46     		mov	r3, r5
 296 01a2 FFF7FEFF 		bl	printf
  66:../src/main.c ****     	if (x==xprev) {
 297              		.loc 1 66 0
 298 01a6 3B68     		ldr	r3, [r7]	@ float
 299 01a8 1846     		mov	r0, r3
 300 01aa F969     		ldr	r1, [r7, #28]	@ float
 301 01ac FFF7FEFF 		bl	__aeabi_fcmpeq
 302 01b0 0346     		mov	r3, r0
 303 01b2 002B     		cmp	r3, #0
 304 01b4 1ED1     		bne	.L7
  67:../src/main.c ****     		printf("Floating point C Iteration: %f\n", x);
  68:../src/main.c ****     		break;
  69:../src/main.c ****     	}
  70:../src/main.c ****     }
 305              		.loc 1 70 0
 306 01b6 B0E7     		b	.L5
 307              	.L9:
 308              		.align	2
 309              	.L8:
 310 01b8 CDCC4C3E 		.word	1045220557
 311 01bc 00000000 		.word	.LC0
 312 01c0 54000000 		.word	.LC1
 313 01c4 5C000000 		.word	.LC2
 314 01c8 60000000 		.word	.LC3
 315 01cc 68000000 		.word	.LC4
 316 01d0 70000000 		.word	.LC5
 317 01d4 A8000000 		.word	.LC6
 318 01d8 AC000000 		.word	.LC7
 319 01dc 0000C842 		.word	1120403456
 320 01e0 00000000 		.word	COUNT
 321 01e4 C8000000 		.word	.LC8
 322 01e8 F8000000 		.word	.LC9
 323 01ec 14010000 		.word	.LC10
 324 01f0 34010000 		.word	.LC11
 325              	.L7:
  67:../src/main.c ****     		printf("Floating point C Iteration: %f\n", x);
 326              		.loc 1 67 0
 327 01f4 3B68     		ldr	r3, [r7]	@ float
 328 01f6 1846     		mov	r0, r3
 329 01f8 FFF7FEFF 		bl	__aeabi_f2d
 330 01fc 0246     		mov	r2, r0
 331 01fe 0B46     		mov	r3, r1
 332 0200 4648     		ldr	r0, .L10
 333 0202 FFF7FEFF 		bl	printf
  68:../src/main.c ****     		break;
 334              		.loc 1 68 0
 335 0206 00BF     		nop
  71:../src/main.c **** 
  72:../src/main.c ****     printf("\nResult: ASM: %f (%d iterations), C floating: %f (%d iterations), Theoretical: %f\n", 
 336              		.loc 1 72 0
 337 0208 B86A     		ldr	r0, [r7, #40]	@ float
 338 020a FFF7FEFF 		bl	__aeabi_f2d
 339 020e 0446     		mov	r4, r0
 340 0210 0D46     		mov	r5, r1
 341 0212 434B     		ldr	r3, .L10+4
 342 0214 1E68     		ldr	r6, [r3]
 343 0216 3B68     		ldr	r3, [r7]	@ float
 344 0218 1846     		mov	r0, r3
 345 021a FFF7FEFF 		bl	__aeabi_f2d
 346 021e 8046     		mov	r8, r0
 347 0220 8946     		mov	r9, r1
 348 0222 786A     		ldr	r0, [r7, #36]	@ float
 349 0224 FFF7FEFF 		bl	__aeabi_f2d
 350 0228 0246     		mov	r2, r0
 351 022a 0B46     		mov	r3, r1
 352 022c 0096     		str	r6, [sp]
 353 022e CDE90289 		strd	r8, [sp, #8]
 354 0232 796B     		ldr	r1, [r7, #52]
 355 0234 0491     		str	r1, [sp, #16]
 356 0236 CDE90623 		strd	r2, [sp, #24]
 357 023a 3A48     		ldr	r0, .L10+8
 358 023c 2246     		mov	r2, r4
 359 023e 2B46     		mov	r3, r5
 360 0240 FFF7FEFF 		bl	printf
  73:../src/main.c **** 
  74:../src/main.c ****     // Calculate error:
  75:../src/main.c ****     float errorAsm = (fabs(perfect-xsol) / (perfect * 1.0)) * 100.0;
 361              		.loc 1 75 0
 362 0244 786A     		ldr	r0, [r7, #36]	@ float
 363 0246 B96A     		ldr	r1, [r7, #40]	@ float
 364 0248 FFF7FEFF 		bl	__aeabi_fsub
 365 024c 0346     		mov	r3, r0
 366 024e 1846     		mov	r0, r3
 367 0250 FFF7FEFF 		bl	__aeabi_f2d
 368 0254 0246     		mov	r2, r0
 369 0256 0B46     		mov	r3, r1
 370 0258 1046     		mov	r0, r2
 371 025a 1946     		mov	r1, r3
 372 025c FFF7FEFF 		bl	fabs
 373 0260 0446     		mov	r4, r0
 374 0262 0D46     		mov	r5, r1
 375 0264 786A     		ldr	r0, [r7, #36]	@ float
 376 0266 FFF7FEFF 		bl	__aeabi_f2d
 377 026a 0246     		mov	r2, r0
 378 026c 0B46     		mov	r3, r1
 379 026e 2046     		mov	r0, r4
 380 0270 2946     		mov	r1, r5
 381 0272 FFF7FEFF 		bl	__aeabi_ddiv
 382 0276 0246     		mov	r2, r0
 383 0278 0B46     		mov	r3, r1
 384 027a 1046     		mov	r0, r2
 385 027c 1946     		mov	r1, r3
 386 027e 4FF00002 		mov	r2, #0
 387 0282 294B     		ldr	r3, .L10+12
 388 0284 FFF7FEFF 		bl	__aeabi_dmul
 389 0288 0246     		mov	r2, r0
 390 028a 0B46     		mov	r3, r1
 391 028c 1046     		mov	r0, r2
 392 028e 1946     		mov	r1, r3
 393 0290 FFF7FEFF 		bl	__aeabi_d2f
 394 0294 0346     		mov	r3, r0
 395 0296 7B61     		str	r3, [r7, #20]	@ float
  76:../src/main.c ****     float errorC = (fabs(perfect-x) / (perfect * 1.0)) * 100.0;
 396              		.loc 1 76 0
 397 0298 3B68     		ldr	r3, [r7]	@ float
 398 029a 786A     		ldr	r0, [r7, #36]	@ float
 399 029c 1946     		mov	r1, r3
 400 029e FFF7FEFF 		bl	__aeabi_fsub
 401 02a2 0346     		mov	r3, r0
 402 02a4 1846     		mov	r0, r3
 403 02a6 FFF7FEFF 		bl	__aeabi_f2d
 404 02aa 0246     		mov	r2, r0
 405 02ac 0B46     		mov	r3, r1
 406 02ae 1046     		mov	r0, r2
 407 02b0 1946     		mov	r1, r3
 408 02b2 FFF7FEFF 		bl	fabs
 409 02b6 0446     		mov	r4, r0
 410 02b8 0D46     		mov	r5, r1
 411 02ba 786A     		ldr	r0, [r7, #36]	@ float
 412 02bc FFF7FEFF 		bl	__aeabi_f2d
 413 02c0 0246     		mov	r2, r0
 414 02c2 0B46     		mov	r3, r1
 415 02c4 2046     		mov	r0, r4
 416 02c6 2946     		mov	r1, r5
 417 02c8 FFF7FEFF 		bl	__aeabi_ddiv
 418 02cc 0246     		mov	r2, r0
 419 02ce 0B46     		mov	r3, r1
 420 02d0 1046     		mov	r0, r2
 421 02d2 1946     		mov	r1, r3
 422 02d4 4FF00002 		mov	r2, #0
 423 02d8 134B     		ldr	r3, .L10+12
 424 02da FFF7FEFF 		bl	__aeabi_dmul
 425 02de 0246     		mov	r2, r0
 426 02e0 0B46     		mov	r3, r1
 427 02e2 1046     		mov	r0, r2
 428 02e4 1946     		mov	r1, r3
 429 02e6 FFF7FEFF 		bl	__aeabi_d2f
 430 02ea 0346     		mov	r3, r0
 431 02ec 3B61     		str	r3, [r7, #16]	@ float
  77:../src/main.c ****     printf("ASM Error: %f%%, C-floating Error: %f%%\n", errorAsm, errorC);
 432              		.loc 1 77 0
 433 02ee 7869     		ldr	r0, [r7, #20]	@ float
 434 02f0 FFF7FEFF 		bl	__aeabi_f2d
 435 02f4 0446     		mov	r4, r0
 436 02f6 0D46     		mov	r5, r1
 437 02f8 3869     		ldr	r0, [r7, #16]	@ float
 438 02fa FFF7FEFF 		bl	__aeabi_f2d
 439 02fe 0246     		mov	r2, r0
 440 0300 0B46     		mov	r3, r1
 441 0302 CDE90023 		strd	r2, [sp]
 442 0306 0948     		ldr	r0, .L10+16
 443 0308 2246     		mov	r2, r4
 444 030a 2B46     		mov	r3, r5
 445 030c FFF7FEFF 		bl	printf
 446              	.L6:
  78:../src/main.c **** 
  79:../src/main.c ****     // Enter an infinite loop, just incrementing a counter
  80:../src/main.c **** 	// This is for convenience to allow registers, variables and memory locations to be inspected at t
  81:../src/main.c **** 	volatile static int loop = 0;
  82:../src/main.c **** 	while (1) {
  83:../src/main.c **** 		loop++;
 447              		.loc 1 83 0 discriminator 1
 448 0310 074B     		ldr	r3, .L10+20
 449 0312 1B68     		ldr	r3, [r3]
 450 0314 5A1C     		adds	r2, r3, #1
 451 0316 064B     		ldr	r3, .L10+20
 452 0318 1A60     		str	r2, [r3]
  84:../src/main.c **** 	}
 453              		.loc 1 84 0 discriminator 1
 454 031a F9E7     		b	.L6
 455              	.L11:
 456              		.align	2
 457              	.L10:
 458 031c 50010000 		.word	.LC12
 459 0320 00000000 		.word	COUNT
 460 0324 70010000 		.word	.LC13
 461 0328 00005940 		.word	1079574528
 462 032c C4010000 		.word	.LC14
 463 0330 00000000 		.word	loop.5035
 464              		.cfi_endproc
 465              	.LFE0:
 467              		.bss
 468              		.align	2
 469              	loop.5035:
 470 0000 00000000 		.space	4
 471              		.text
 472              	.Letext0:
DEFINED SYMBOLS
                            *ABS*:00000000 main.c
                            *COM*:00000004 COUNT
     /tmp/cc0zxYn5.s:20     .rodata:00000000 $d
     /tmp/cc0zxYn5.s:82     .text.main:00000000 $t
     /tmp/cc0zxYn5.s:87     .text.main:00000000 main
     /tmp/cc0zxYn5.s:310    .text.main:000001b8 $d
     /tmp/cc0zxYn5.s:327    .text.main:000001f4 $t
     /tmp/cc0zxYn5.s:458    .text.main:0000031c $d
     /tmp/cc0zxYn5.s:469    .bss:00000000 loop.5035
     /tmp/cc0zxYn5.s:468    .bss:00000000 $d
                     .debug_frame:00000010 $d
                           .group:00000000 wm4.1.5e399dbc69f224bb380807870f7721cc
                           .group:00000000 wm4.newlib.h.8.384a112feabb3bef7b573ae48cde2e3b
                           .group:00000000 wm4.features.h.22.6a4ca7cd053637cc1d0db6c16f39b2d7
                           .group:00000000 wm4.config.h.212.4163ef2871a828c674038d036b081cfd
                           .group:00000000 wm4._ansi.h.23.5644b60c990a4800b02a6e654e88f93a
                           .group:00000000 wm4.stdio.h.31.7c0e28c411445f3f9c5b11accf882760
                           .group:00000000 wm4.stddef.h.184.159df79b4ca79c76561572a55985524c
                           .group:00000000 wm4.stdarg.h.34.3a23a216c0c293b3d2ea2e89281481e6
                           .group:00000000 wm4.stddef.h.39.7e3d906ac58942e0b374c527445f5de5
                           .group:00000000 wm4._default_types.h.6.5e12cd604db8ce00b62bb2f02708eaf3
                           .group:00000000 wm4.lock.h.2.9bc98482741e5e2a9450b12934a684ea
                           .group:00000000 wm4._types.h.54.d3d34a3b7f3cc230cd159baf022b4b08
                           .group:00000000 wm4.stddef.h.158.61317cdbfb4026324507d123a50b0fd6
                           .group:00000000 wm4.reent.h.17.8bd9e4098e0428508c282cad794fae43
                           .group:00000000 wm4.types.h.23.0d949686bbcadd1621462d4fa1f884f9
                           .group:00000000 wm4.types.h.2.e9cec8c90ab35f77d9f499e06ae02400
                           .group:00000000 wm4.types.h.80.5759fe41f87f5211a5952dfb1fc64f19
                           .group:00000000 wm4.stdio.h.2.4aa87247282eca6c8f36f9de33d8df1a
                           .group:00000000 wm4.stdio.h.64.0f05fa42cfe2db7fc44729a52d3ba948
                           .group:00000000 wm4.math.h.35.13c5bec129ef04ad1bb0a1152b29c624

UNDEFINED SYMBOLS
__aeabi_fmul
__aeabi_f2iz
__aeabi_i2f
__aeabi_fdiv
__aeabi_f2d
__aeabi_i2d
__aeabi_dadd
__aeabi_ddiv
__aeabi_d2f
__aeabi_fadd
__aeabi_fcmpeq
__aeabi_fsub
__aeabi_dmul
printf
scanf
optimize
fabs
